<style>

    #chart_div{
        position:relative;
        left:12;
        top:-12;
        z-index:0;
    }
    
      @font-face {
        font-family: "myFonts";
        src: url("http://api.zonamahasiswa.id/assets/fonts/NotoSans-Bold.ttf");

    }
    
        @font-face {
        font-family: "arialRoundet";
        src: url("http://api.zonamahasiswa.id/assets/fonts/arialroundet.ttf");

    }
    
    .judul{
        margin:0 auto;
        padding-top:2vh;
        display:table;
        z-index:1;
        position:relative;
        color:#3e4042;
        font-family:arialRoundet;
    }

</style>
<html style="height:100vh; width:100vw;">
  <head>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Pilihan', 'Suara',{role: 'style'},{role: 'annotation' }],
        //  ['UIN Maliki Malang',  1000],
          <?php
            for($i=0;$i<count($data);$i++)
            {
            ?>
                ['<?php echo $data[$i]['nama_tokoh']?>',<?php echo $data[$i]['total_vote']?>,'<?php echo $color[$i]?>','<?php echo $data[$i]['total_vote']?>'],
                <?php
                
            }
          ?>
          
        ]);
        var options = {
            annotations: {
                stemColor : 'none'
            },
           animation: {
                duration: 200,
                easing: 'out',
                startup: true
            },  
          legend: 'none',
          chartArea: {
                left:130,
                top:40,
                width: '100%',
                height: '80%'
            },
            vAxis: {
            scaleType : 'log',
            textStyle : {
                fontSize: 10, // or the number you want
                //color:"#8d9396",
                color:"#545759",
                fontName:'arialRoundet'
            },
            gridlines: {
                color: 'transparent'
            }
            
        },
         hAxis: {
              //baselineColor : '#adb1b4',
              //baselineColor : '#45484a',
               baselineColor : '#e6ebf0',
              gridlines: {
                color: 'transparent'
            },
            textPosition: 'none'
         }
        
        };
        var chart = new google.visualization.BarChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
    </script>
  </head>
  <body>
    <span class="judul"><strong>Perolehan Voting Sementara</strong></span>  
    <div id="chart_div" style="height:100vh; width:90%;"></div>
  </body>
</html>