<?php
class Video_model extends CI_Model
{

    public function get_data_user_by_person($person_id)
    {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('user.person_id', $person_id);
        return $this->db->get()->result_array();
    }

    public function count_video($id_categori = null)
    {
        $this->db->select('COUNT(video.video_id) as id');
        $this->db->from('video');
        $this->db->join('video_category', 'video_category.video_category_id=video.video_category_id', 'left');
        if ($id_categori != null) {
            $this->db->where('video.video_category_id', $id_categori);
        }
        $query = $this->db->get();
        // return $query->num_rows();
        // return $this->db->last_query();
        // return $this->db->count_all_results();
        return $query->row_array()['id'];
    }

    public function video($id_user = null, $video_id = null, $category_id = null, $category_sub_id = null, $next_page = null)
    {
        if ($id_user != null) {
            $this->db->select('video.*, user.*, video_like.video_like_id, video_save.video_save_id, video_view.video_view_id, video_category.video_category_nama, video_category_sub.video_category_sub_nama');
            $this->db->join('video_like', 'video_like.video_id=video.video_id AND video_like.user_id=' . $id_user . '', 'left');
            $this->db->join('video_save', 'video_save.video_id=video.video_id AND video_save.user_id=' . $id_user . '', 'left');
            $this->db->join('video_view', 'video_view.video_id=video.video_id AND video_view.user_id=' . $id_user . '', 'left');
        } else {
            $this->db->select('video.*, user.*, video_category.video_category_nama, video_category_sub.video_category_sub_nama');
        }
        $this->db->from('video');
        $this->db->join('video_category', 'video_category.video_category_id=video.video_category_id', 'left');
        $this->db->join('video_category_sub', 'video_category_sub.video_category_sub_id=video.video_category_sub_id', 'left');
        $this->db->join('user', 'video.user_id=user.person_id', 'left');
        $this->db->where('video.video_publish_status', '1');
        if ($category_id != null) {
            $this->db->where('video.video_category_id', $category_id);
        }
        if ($category_sub_id != null) {
            $this->db->where('video.video_category_sub_id', $category_sub_id);
        }
        if ($video_id != null) {
            $this->db->where('video.video_id <=', $video_id);
            $limit = 100;
            $awalData = $video_id;
            $this->db->limit($limit);
        } else if ($next_page != null) {
            $this->db->where('video.video_id <=', $next_page - 1);
            $limit = 100;
            // $awalData = ($limit * $next_page) - $limit;
            $awalData = $next_page;
            $this->db->limit($limit);
        } else {
            $limit = 100;
            $this->db->limit($limit);
        }
        $this->db->order_by('video.video_id', 'DESC');
        $query = $this->db->get();
        return $query->result_array();
        // return $query->result();

        // return $query->num_rows();
        return $this->db->last_query();
        // return $this->db->count_all_results();
        // return $query;
    }

    public function video_popular($id_user = null, $video_id = null, $category_id = null, $category_sub_id = null, $next_page = 1)
    {
        $limit = 100;
        $page = $next_page;
        $start = ($page > 1) ? ($page * $limit) - $limit : 0;
        if ($id_user != null) {
            $this->db->select('video.*, user.*, video_like.video_like_id, video_save.video_save_id, video_view.video_view_id, video_category.video_category_nama, video_category_sub.video_category_sub_nama');
            $this->db->join('video_like', 'video_like.video_id=video.video_id AND video_like.user_id=' . $id_user . '', 'left');
            $this->db->join('video_save', 'video_save.video_id=video.video_id AND video_save.user_id=' . $id_user . '', 'left');
            $this->db->join('video_view', 'video_view.video_id=video.video_id AND video_view.user_id=' . $id_user . '', 'left');
        } else {
            $this->db->select('video.*, user.*, video_category.video_category_nama, video_category_sub.video_category_sub_nama');
        }
        $this->db->from('video');
        $this->db->join('video_category', 'video_category.video_category_id=video.video_category_id', 'left');
        $this->db->join('video_category_sub', 'video_category_sub.video_category_sub_id=video.video_category_sub_id', 'left');
        $this->db->join('user', 'video.user_id=user.person_id', 'left');
        $this->db->where('video.video_publish_status', '1');
        // if ($category_id != null) {
        //     $this->db->where('video.video_category_id', $category_id);
        // }
        // if ($category_sub_id != null) {
        //     $this->db->where('video.video_category_sub_id', $category_sub_id);
        // }
        // if ($video_id != null) {
        //     $this->db->where('video.video_id <=', $video_id);
        //     $limit = 2;
        //     $awalData = $video_id;
        //     $this->db->limit($limit);
        // } else if ($next_page != null) {
        //     $this->db->where('video.video_id <=', $next_page - 1);
        //     // $awalData = ($limit * $next_page) - $limit;
        //     $awalData = $next_page;
        //     $this->db->limit($limit);
        // } else {
        //     $this->db->limit($limit, $start);
        // }

        $this->db->limit($limit, $start);
        $this->db->order_by('video.video_rate', 'DESC');
        $query = $this->db->get();
        return $query->result_array();
        // return $query->result();

        // return $query->num_rows();
        return $this->db->last_query();
        // return $this->db->count_all_results();
        // return $query;
    }

    public function video_update($id, $data)
    {
        $this->db->where('video_id', $id);
        return $this->db->update('video', $data);
    }

    public function video_comment($id_user = null, $video_id = null, $next_page = null, $id_comment = null)
    {
        // $limit = 10;
        // $awalData = ($limit * $halamanaktif) - $limit;
        $this->db->select('video_comment.*, user.id_user, user.person_id, user.person_name, user.person_given_name, user.person_family_name, user.person_photo, video_comment_like.video_comment_like_id');
        $this->db->from('video_comment');
        $this->db->join('user', 'video_comment.user_id=user.person_id', 'left');
        $this->db->join('video_comment_like', 'video_comment.video_comment_id=video_comment_like.video_comment_id AND video_comment_like.user_id=' . $id_user, 'left');
        // $this->db->where('video_comment.user_id', $id_user);
        $this->db->where('video_comment.video_id', $video_id);
        if ($id_comment == null) {
            $this->db->where('video_comment.video_comment_status', 'message');
        } else {
            $this->db->where('video_comment.video_comment_reply_id', $id_comment);
            $this->db->where('video_comment.video_comment_status', 'reply');
        }
        // if ($halamanaktif != null) {
        //     $this->db->limit($limit, $awalData);
        // }

        if ($next_page != null) {
            $this->db->where('video_comment.video_comment_id <=', $next_page);
            $limit = 10;
            $awalData = $next_page;
            $this->db->limit($limit);
        } else {
            $limit = 10;
            $this->db->limit($limit);
        }
        $this->db->order_by('video_comment.video_comment_id', 'DESC');
        $query = $this->db->get();
        return $query->result_array();

        // return $query->num_rows();
        return $this->db->last_query();
        // return $this->db->count_all_results();
        // return $query;
    }


    public function video_comment_by_id($comment_id)
    {
        $this->db->select('*');
        $this->db->from('video_comment');
        $this->db->where('video_comment_id', $comment_id);
        $query = $this->db->get();
        // return $this->db->last_query();
        return $query->result_array();
    }

    public function video_comment_reply($id_user = null, $video_id = null, $next_page = null, $id_comment = null)
    {
        $this->db->select('video_comment.*, user.id_user, user.person_id, user.person_name, user.person_given_name, user.person_family_name, user.person_photo, video_comment_like.video_comment_like_id');
        $this->db->from('video_comment');
        $this->db->join('user', 'video_comment.user_id=user.person_id', 'left');
        $this->db->join('video_comment_like', 'video_comment.video_comment_id=video_comment_like.video_comment_id AND video_comment_like.user_id=' . $id_user, 'left');
        $this->db->where('video_comment.video_id', $video_id);
        $this->db->where('video_comment.video_comment_status', 'reply');
        if ($next_page != null) {
            $this->db->where('video_comment.video_comment_id <=', $next_page);
            $limit = 10;
            $awalData = $next_page;
            $this->db->limit($limit);
        } else {
            $limit = 10;
            $this->db->limit($limit);
        }
        $this->db->order_by('video_comment.video_comment_id', 'DESC');
        $query = $this->db->get();
        return $query->result_array();

        // return $query->num_rows();
        return $this->db->last_query();
        // return $this->db->count_all_results();
        // return $query;
    }

    public function video_comment_update($id, $data)
    {
        $this->db->where('video_comment_id', $id);
        return $this->db->update('video_comment', $data);
    }

    public function video_comment_store($data)
    {
        $this->db->insert('video_comment', $data);
        return $this->db->insert_id();
    }

    public function video_comment_count($id_user = null, $id)
    {
        $this->db->select('COUNT(video_comment_id) as total');
        $this->db->where('video_id', $id);
        if ($id_user != null) {
            $this->db->where('user_id', $id_user);
        }
        $this->db->from('video_comment');
        return $this->db->get()->result_array()[0]['total'];
    }

    public function video_comment_count_level_one($id)
    {
        $this->db->select('COUNT(video_comment_id) as total');
        $this->db->where('video_id', $id);
        $this->db->where('video_comment_status', 'message');
        $this->db->from('video_comment');
        // return $this->db->get()->last_query();
        return $this->db->get()->result_array()[0]['total'];
    }

    public function video_comment_last($id_user = null, $video_id = null, $id_comment = null)
    {
        // $limit = 10;
        // $awalData = ($limit * $halamanaktif) - $limit;
        $this->db->select('video_comment.*, user.id_user, user.person_id, user.person_name, user.person_given_name, user.person_family_name, user.person_photo, video_comment_like.video_comment_like_id');
        $this->db->from('video_comment');
        $this->db->join('user', 'video_comment.user_id=user.person_id', 'left');
        $this->db->join('video_comment_like', 'video_comment.video_comment_id=video_comment_like.video_comment_id AND video_comment_like.user_id=' . $id_user, 'left');
        // $this->db->where('video_comment.user_id', $id_user);
        $this->db->where('video_comment.video_id', $video_id);
        if ($id_comment == null) {
            $this->db->where('video_comment.video_comment_status', 'message');
        } else {
            $this->db->where('video_comment.video_comment_reply_id', $id_comment);
            $this->db->where('video_comment.video_comment_status', 'reply');
        }
        $this->db->limit(1);
        $this->db->order_by('video_comment.video_comment_id', 'DESC');
        $query = $this->db->get();
        return $query->result_array();

        // return $query->num_rows();
        return $this->db->last_query();
        // return $this->db->count_all_results();
        // return $query;
    }

    public function video_like($data)
    {
        $this->db->select('COUNT(video_like_id) as total');
        $this->db->from('video_like');
        $this->db->where('video_id', $data['video_id']);
        $this->db->where('user_id', $data['user_id']);
        $query = $this->db->get();
        // return $this->db->last_query();
        return $query->result();
    }

    public function video_like_store($data)
    {
        return $this->db->insert('video_like', $data);
    }

    public function video_like_delete($data)
    {
        $this->db->where('video_id', $data['video_id']);
        $this->db->where('user_id', $data['user_id']);
        return $this->db->delete('video_like');
    }

    public function video_comment_like($data)
    {
        $this->db->select('COUNT(video_comment_like_id) as total');
        $this->db->from('video_comment_like');
        $this->db->where('video_comment_id', $data['video_comment_id']);
        $this->db->where('user_id', $data['user_id']);
        $query = $this->db->get();
        // return $this->db->last_query();
        return $query->result();
    }

    public function video_comment_like_store($data)
    {
        return $this->db->insert('video_comment_like', $data);
    }

    public function video_comment_like_delete($data)
    {
        $this->db->where('video_comment_id', $data['video_comment_id']);
        $this->db->where('user_id', $data['user_id']);
        return $this->db->delete('video_comment_like');
    }

    public function video_like_count($id_user = null, $id)
    {
        $this->db->select('COUNT(video_like_id) as total');
        $this->db->where('video_id', $id);
        if ($id_user != null) {
            $this->db->where('user_id', $id_user);
        }
        $this->db->from('video_like');
        return $this->db->get()->result_array()[0]['total'];
    }

    public function video_comment_like_count($id_user = null, $id)
    {
        $this->db->select('COUNT(video_comment_like_id) as total');
        $this->db->where('video_comment_id', $id);
        if ($id_user != null) {
            $this->db->where('user_id', $id_user);
        }
        $this->db->from('video_comment_like');
        return $this->db->get()->result_array()[0]['total'];
    }

    public function video_share_store($data)
    {
        return $this->db->insert('video_share', $data);
    }

    public function video_share_count($id_user = null, $video_id)
    {
        $this->db->select('COUNT(video_share_id) as total');
        $this->db->where('video_id', $video_id);
        if ($id_user != null) {
            $this->db->where('user_id', $id_user);
        }
        $this->db->from('video_share');
        // $this->db->get();
        // return $this->db->last_query();
        return $this->db->get()->result_array()[0]['total'];
    }

    public function video_category()
    {
        $this->db->from('video_category');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function video_category_sub($category_id)
    {
        $this->db->select('*');
        $this->db->from('video_category_sub');
        $this->db->where('video_category_id', $category_id);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function video_save($data)
    {
        $this->db->select('video_save.*,video.*,video_category.video_category_nama,video_category_sub.video_category_sub_nama');
        $this->db->from('video_save');
        $this->db->join('video', 'video_save.video_id=video.video_id');
        $this->db->join('video_category', 'video.video_category_id=video_category.video_category_id', 'left');
        $this->db->join('video_category_sub', 'video.video_category_sub_id=video_category_sub.video_category_sub_id', 'left');
        // if (isset($data['video_id'])) {
        //     $this->db->where('video_id', $data['video_id']);
        // }
        $this->db->where('video_save.user_id', $data['user_id']);
        $this->db->order_by('video_save.video_save_date', 'DESC');
        $query = $this->db->get();
        // return $this->db->last_query();
        return $query->result();
    }

    public function video_save_store($data)
    {
        return $this->db->insert('video_save', $data);
    }

    public function video_save_delete($data)
    {
        $this->db->where('video_id', $data['video_id']);
        $this->db->where('user_id', $data['user_id']);
        return $this->db->delete('video_save');
    }

    public function video_save_count($id_user = null, $id)
    {
        $this->db->select('COUNT(video_save_id) as total');
        $this->db->where('video_id', $id);
        if ($id_user != null) {
            $this->db->where('user_id', $id_user);
        }
        $this->db->from('video_save');
        return $this->db->get()->result_array()[0]['total'];
    }

    public function video_view_store($data)
    {
        return $this->db->insert('video_view', $data);
    }

    public function video_view_delete($data)
    {
        $this->db->where('video_view_id', $data['video_view_id']);
        $this->db->where('user_id', $data['user_id']);
        return $this->db->delete('video_view');
    }

    public function video_view_count($id_user = null, $id_video)
    {
        $this->db->select('COUNT(video_view_id) as total');
        $this->db->where('video_id', $id_video);
        if ($id_user != null) {
            $this->db->where('user_id', $id_user);
        }
        $this->db->from('video_view');
        return $this->db->get()->result_array()[0]['total'];
    }

    public function video_play($data)
    {
        $this->db->select('video_play.*, video.video_duration');
        $this->db->from('video_play');
        $this->db->join('video', 'video_play.video_id=video.video_id', 'left');
        $this->db->where('video_play.video_id', $data['video_id']);
        $this->db->where('video_play.user_id', $data['user_id']);
        $query = $this->db->get();
        // return $this->db->last_query();
        return $query->result();
    }

    public function video_play_store($data)
    {
        return $this->db->insert('video_play', $data);
    }

    public function video_play_update($data, $id)
    {
        $this->db->where('video_id', $id['video_id']);
        $this->db->where('user_id', $id['user_id']);
        return $this->db->update('video_play', $data);
    }

    public function video_play_delete($data)
    {
        $this->db->where('video_id', $data['video_id']);
        $this->db->where('user_id', $data['user_id']);
        return $this->db->delete('video_play');
    }

    public function video_play_count($id_user = null, $id_video)
    {
        $this->db->select('COUNT(video_play_id) as total');
        $this->db->where('video_id', $id_video);
        if ($id_user != null) {
            $this->db->where('user_id', $id_user);
        }
        $this->db->from('video_play');
        return $this->db->get()->result_array()[0]['total'];
    }

    public function video_duration_count($id_user = null, $id_video)
    {
        $this->db->select('SUM(video_play_total) as total');
        $this->db->where('video_id', $id_video);
        if ($id_user != null) {
            $this->db->where('user_id', $id_user);
        }
        $this->db->from('video_play');
        return $this->db->get()->result_array()[0]['total'];
    }

    public function video_notification($user_id = null)
    {
        $this->db->select('video_notification.*, user.*, video.*');
        $this->db->from('video_notification');
        // if ($user_id != null) {
        //     $this->db->join('video_notification_read', 'video_notification.video_notification_id=video_notification_read.video_notification_id AND video_notification_read.user_id=' . $user_id, 'left');
        // }
        $this->db->join('user', 'video_notification.from_person_id=user.person_id', 'left');
        $this->db->join('video', 'video_notification.video_id=video.video_id', 'left');
        if ($user_id != null) {
            $this->db->where("video_notification.person_id=$user_id AND video_notification.from_person_id!=$user_id  AND video_notification.from_person_id!=$user_id");
            $this->db->or_where("video_notification.person_id='0' AND video_notification.video_notification_type LIKE '%video%' AND video_notification.from_person_id!=$user_id");
        }
        $this->db->order_by('video_notification.video_notification_id', 'DESC');
        $query = $this->db->get();
        // return $this->db->last_query();
        return $query->result_array();
    }

    public function video_notification_read($user_id = null, $notif_id = null)
    {
        $this->db->select('*');
        $this->db->from('video_notification_read');
        if ($user_id != null) {
            $this->db->where('user_id', $user_id);
        }
        if ($notif_id != null) {
            $this->db->where('video_notification_id', $notif_id);
        }
        $query = $this->db->get();
        return $this->db->last_query();
        return $query->result_array();
    }

    public function video_notification_store($data)
    {
        return $this->db->insert('video_notification', $data);
    }

    public function video_notification_count($user_id, $video_id, $type)
    {
        $this->db->select('COUNT(`video_notification_id`) as total');
        $this->db->from('video_notification');
        $this->db->where('person_id', $user_id);
        $this->db->where('video_id', $video_id);
        $this->db->where('video_notification_type', $type);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function video_rate($id, $date = null)
    {
        $d = explode(' ', $date);
        $this->db->select('*');
        $this->db->from('video_rate');
        $this->db->where('video_id', $id);
        if ($date != null) {
            $this->db->where("video_rate_date LIKE '%$d[0]%'");
        }
        $query = $this->db->get();
        // return $this->db->last_query();
        return $query->result_array();
    }

    public function video_rate_count($id)
    {
        $this->db->select('SUM(video_rate_count) as total');
        $this->db->from('video_rate');
        $this->db->where('video_id', $id);
        return $this->db->get()->result_array()[0]['total'];
    }

    public function video_rate_store($data)
    {
        return $this->db->insert('video_rate', $data);
    }

    public function video_rate_update($id_rate, $date, $data)
    {
        $this->db->where('video_rate_id', $id_rate);
        $this->db->where('video_rate_id LIKE', "%$date%");
        return $this->db->update('video_rate', $data);
    }

    public function video_open_notif($user_id = null)
    {
        $this->db->select('*');
        $this->db->from('video_open_notif');
        if ($user_id != null) {
            $this->db->where('user_id', $user_id);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    public function video_notif_count($user_id = null, $date = null)
    {
        $this->db->select('COUNT(video_notification.video_notification_id) as total');
        $this->db->from('video_notification');
        // if ($user_id != null) {
        //     $this->db->join('video_notification_read', 'video_notification.video_notification_id=video_notification_read.video_notification_id AND video_notification_read.user_id=' . $user_id, 'left');
        // }
        // $this->db->join('user', 'video_notification.from_person_id=user.person_id', 'left');
        // $this->db->join('video', 'video_notification.video_id=video.video_id', 'left');
        $this->db->where("video_notification.person_id='$user_id' AND video_notification.video_notification_date>='$date' AND video_notification.from_person_id!=$user_id");
        $this->db->or_where("video_notification.person_id='0' AND video_notification.video_notification_type LIKE '%video%' AND video_notification.video_notification_date>='$date' AND video_notification.from_person_id!=$user_id");
        // $this->db->order_by('video_notification.video_notification_id', 'DESC');
        $query = $this->db->get();
        // return $this->db->last_query();
        return $query->result_array()[0]['total'];
    }

    public function video_open_notif_store($data)
    {
        return $this->db->insert('video_open_notif', $data);
    }

    public function video_open_notif_update($user_id, $data)
    {
        $this->db->where('user_id', $user_id);
        return $this->db->update('video_open_notif', $data);
    }
    
     public function video_notification_count_liked($user_id, $video_id, $type, $comment_id)
    {
        $this->db->select('COUNT(`video_notification_id`) as total');
        $this->db->from('video_notification');
        $this->db->where('person_id', $user_id);
        $this->db->where('video_id', $video_id);
        $this->db->where('comment_id', $comment_id);
        $this->db->where('video_notification_type', $type);
        $query = $this->db->get();
        return $query->result_array();
    }
}