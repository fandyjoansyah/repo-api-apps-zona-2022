<?php



class All_model extends CI_Model{


   // Regis -> cek user by email
   public function get_post_by_date(){
      $this->db->select('wp.ID, wp.post_author, wp.post_date, wp.post_title, wp.guid, wp.post_content, wpm2.meta_value, t2.display_name as user_login, c.name, c.term_id, d.gambar_user_wp');
      $this->db->from('wpuf_posts wp');

      $this->db->join('wpuf_postmeta wpm', 'wp.ID = wpm.post_id '); // get thumnile post
      $this->db->join('wpuf_postmeta wpm2', 'wpm.meta_value = wpm2.post_id'); // get thumnile post
      $this->db->join('wpuf_users t2', 't2.ID = wp.post_author'); // get name author

      $this->db->join('wpuf_term_relationships cr', 'wp.ID = cr.object_id'); // gcategori
      $this->db->join('wpuf_term_taxonomy ct', 'ct.term_taxonomy_id = cr.term_taxonomy_id'); // gcategori
      $this->db->join('wpuf_terms c', 'ct.term_id = c.term_id'); // gcategori

      $this->db->join('donatur d', 'wp.post_author = d.id_user_wp'); // gambar_autor

      $this->db->order_by('wp.post_date','DESC');

      $this->db->where('wpm.meta_key','_thumbnail_id');
      $this->db->where('wpm2.meta_key','_wp_attached_file');
      $this->db->where('wp.post_status','publish');
      $this->db->where('wp.post_type','post');

      $this->db->where('ct.taxonomy','category'); // categori

      $query = $this->db->get();
      return $query->result(); 

     
  }

  
  
  
  public function get_tag_by_id_post($id_post){
   $this->db->select('p.id, t.name');
   $this->db->from('wpuf_posts p');

   $this->db->join('wpuf_term_relationships cr', 'p.id = cr.object_id'); 
   $this->db->join('wpuf_term_taxonomy ct', 'ct.term_taxonomy_id = cr.term_taxonomy_id');
   $this->db->join('wpuf_terms c', 'ct.term_id = c.term_id');
   $this->db->join('wpuf_term_relationships tr', 'p.id = tr.object_id');
   $this->db->join('wpuf_term_taxonomy tt', 'tt.term_taxonomy_id = tr.term_taxonomy_id');
   $this->db->join('wpuf_terms t', 'tt.term_id = t.term_id');

   
   $this->db->where('ct.taxonomy','category'); // categori
   $this->db->where('tt.taxonomy','post_tag'); // TAG
   
   $this->db->where('p.ID',$id_post); // TAG

   $query = $this->db->get();
   return $query->result(); 

}

public function get_post_by_id_categori($id_categori){
   $this->db->select('wp.ID, wp.post_author, wp.post_date, wp.post_title, wp.guid, wp.post_content, wpm2.meta_value, t2.display_name as user_login, c.name, c.term_id, d.gambar_user_wp');
   $this->db->from('wp_posts wp');

   $this->db->join('wp_postmeta wpm', 'wp.ID = wpm.post_id '); // get thumnile post
   $this->db->join('wp_postmeta wpm2', 'wpm.meta_value = wpm2.post_id'); // get thumnile post
   $this->db->join('wp_users t2', 't2.ID = wp.post_author'); // get name author

   $this->db->join('wp_term_relationships cr', 'wp.ID = cr.object_id'); // gcategori
   $this->db->join('wp_term_taxonomy ct', 'ct.term_taxonomy_id = cr.term_taxonomy_id'); // gcategori
   $this->db->join('wp_terms c', 'ct.term_id = c.term_id'); // gcategori

   $this->db->join('donatur d', 'wp.post_author = d.id_user_wp'); // gambar_autor

   $this->db->order_by('wp.post_date','DESC');

   $this->db->where('wpm.meta_key','_thumbnail_id');
   $this->db->where('wpm2.meta_key','_wp_attached_file');
   $this->db->where('wp.post_status','publish');
   $this->db->where('wp.post_type','post');

   $this->db->where('ct.taxonomy','category'); // categori
   $this->db->where('c.term_id',$id_categori); // categori
   $query = $this->db->get();
   return $query->num_rows(); 

  
}


public function get_search($key_search){
   $this->db->select('wp.ID, wp.post_author, wp.post_date, wp.post_title, wp.guid, wp.post_content, wpm2.meta_value, t2.display_name as user_login, c.name, c.term_id, d.gambar_user_wp');
   $this->db->from('wpuf_posts wp');

   $this->db->join('wpuf_postmeta wpm', 'wp.ID = wpm.post_id '); // get thumnile post
   $this->db->join('wpuf_postmeta wpm2', 'wpm.meta_value = wpm2.post_id'); // get thumnile post
   $this->db->join('wpuf_users t2', 't2.ID = wp.post_author'); // get name author

   $this->db->join('wpuf_term_relationships cr', 'wp.ID = cr.object_id'); // gcategori
   $this->db->join('wpuf_term_taxonomy ct', 'ct.term_taxonomy_id = cr.term_taxonomy_id'); // gcategori
   $this->db->join('wpuf_terms c', 'ct.term_id = c.term_id'); // gcategori

   $this->db->join('donatur d', 'wp.post_author = d.id_user_wp'); // gambar_autor

   $this->db->order_by('wp.post_date','DESC');

   $this->db->where('wpm.meta_key','_thumbnail_id');
   $this->db->where('wpm2.meta_key','_wp_attached_file');
   $this->db->where('wp.post_status','publish');
   $this->db->where('wp.post_type','post');

   $this->db->where('ct.taxonomy','category'); // categori
   $this->db->like('wp.post_title', $key_search);

   $query = $this->db->get();
   return $query->result(); 

  
}


public function get_post_poluler(){
   
      $this->db->select('wp.ID, wp.post_author, wp.post_date, wp.post_title, wp.guid, wp.post_content, wpm2.meta_value, wpm3.meta_value AS view, t2.display_name as user_login, c.name, c.term_id, d.gambar_user_wp');
      $this->db->from('wpuf_posts wp');

      $this->db->join('wpuf_postmeta wpm', 'wp.ID = wpm.post_id '); // get thumnile post
      $this->db->join('wpuf_postmeta wpm2', 'wpm.meta_value = wpm2.post_id'); // get thumnile post
      $this->db->join('wpuf_users t2', 't2.ID = wp.post_author'); // get name author

      $this->db->join('wpuf_term_relationships cr', 'wp.ID = cr.object_id'); // gcategori
      $this->db->join('wpuf_term_taxonomy ct', 'ct.term_taxonomy_id = cr.term_taxonomy_id'); // gcategori
      $this->db->join('wpuf_terms c', 'ct.term_id = c.term_id'); // gcategori

      $this->db->join('donatur d', 'wp.post_author = d.id_user_wp'); // gambar_autor

      $this->db->join('wpuf_postmeta wpm3', 'wp.ID = wpm3.post_id '); // view

      
      $this->db->order_by('wp.post_date','DESC');

      $this->db->where('wpm.meta_key','_thumbnail_id');
      $this->db->where('wpm2.meta_key','_wp_attached_file');
      $this->db->where('wpm3.meta_key','post_views_count');
      $this->db->where('wp.post_status','publish');
      $this->db->where('wp.post_type','post');

      $this->db->where('ct.taxonomy','category'); // categori

      $this->db->limit(5);

      $query = $this->db->get();
      return $query->result(); 

     
  }


  public function getdate(){
   $this->db->select('wp.ID, wp.post_author, wp.post_date, wp.post_title, wp.guid, wp.post_content, wpm2.meta_value, wpm3.meta_value AS view, t2.display_name as user_login, c.name, c.term_id, d.gambar_user_wp');
   $this->db->from('wpuf_posts wp');

   $this->db->join('wpuf_postmeta wpm', 'wp.ID = wpm.post_id AND wpm.meta_key = \'_thumbnail_id\''); // get thumnile post
   $this->db->join('wpuf_postmeta wpm2', 'wpm.meta_value = wpm2.post_id AND wpm2.meta_key = \'_wp_attached_file\''); // get thumnile post
   $this->db->join('wpuf_users t2', 't2.ID = wp.post_author'); // get name author

   $this->db->join('wpuf_term_relationships cr', 'wp.ID = cr.object_id'); // gcategori
   $this->db->join('wpuf_term_taxonomy ct', 'ct.term_taxonomy_id = cr.term_taxonomy_id'); // gcategori
   $this->db->join('wpuf_terms c', 'ct.term_id = c.term_id'); // gcategori

   $this->db->join('donatur d', 'wp.post_author = d.id_user_wp'); // gambar_autor

   $this->db->join('wpuf_postmeta wpm3', 'wp.ID = wpm3.post_id AND wpm3.meta_key = \'post_views_count\''); // view

   
   $this->db->order_by('wpm3.meta_value','DESC');

//      $this->db->where('wpm.meta_key','_thumbnail_id');
//      $this->db->where('wpm2.meta_key','_wp_attached_file');
//      $this->db->where('wpm3.meta_key','post_views_count');
   //$this->db->where('wp.post_status','publish');
   //$this->db->where('wp.post_type','post');
//      $this->db->where('wp.post_date >=', date(2021-01-01);
   
   $this->db->where('ct.taxonomy','category'); // categori

   //$this->db->where('wp.post_date >=',DATE('2021-01-01')); // tglpertama
   $this->db->where('wp.post_date <=','2021-01-01'); // tglkedua
   $this->db->where('wp.post_date >=','2019-01-01'); // tglkedua

   $query = $this->db->get();
   return $query->result();  

  }


   // cek email apakah sudah ada ?
   public function getUseremail($person_email){

      $query= $this->db->get_where('user', [ 'person_email' => $person_email ]);

      foreach ($query->result_array() as $row)
          {
                  $id= $row['person_token'];
                  return $id;
          }

          
  }

  public function get_data_user($person_email){
   $this->db->select('*');
   $this->db->from('user');
   $this->db->where('user.person_email',$person_email);
   return $query = $this->db->get()->result_array();
   }


   // Update Token
   public function update_token($personId, $data){

      $this->db->where('person_id', $personId);
      return $this->db->update('user',$data);

          
  }

  public function createUser($data){
   $this->db->insert('user', $data);
   return $this->db->affected_rows();
return $this->db->_error_message(); 
return $this->db->_error_number();
}


public function get_post_by_date_fix(){
   $this->db->select('wp.ID, wp.post_author, wp.post_date, wp.post_title, wp.guid, wp.post_content, wpm2.meta_value, t2.display_name as user_login, c.name, c.term_id, d.gambar_user_wp');
   $this->db->from('wpuf_posts wp');

   $this->db->join('wpuf_postmeta wpm', 'wp.ID = wpm.post_id '); // get thumnile post
   $this->db->join('wpuf_postmeta wpm2', 'wpm.meta_value = wpm2.post_id'); // get thumnile post
   $this->db->join('wpuf_users t2', 't2.ID = wp.post_author'); // get name author

   $this->db->join('wpuf_term_relationships cr', 'wp.ID = cr.object_id'); // gcategori
   $this->db->join('wpuf_term_taxonomy ct', 'ct.term_taxonomy_id = cr.term_taxonomy_id'); // gcategori
   $this->db->join('wpuf_terms c', 'ct.term_id = c.term_id'); // gcategori

   
   $this->db->join('donatur d', 'wp.post_author = d.id_user_wp'); // gambar_autor

   $this->db->order_by('wp.post_date','DESC');

   $this->db->where('wpm.meta_key','_thumbnail_id');
   $this->db->where('wpm2.meta_key','_wp_attached_file');
   $this->db->where('wp.post_status','publish');
   $this->db->where('wp.post_type','post');

   $this->db->where('ct.taxonomy','category'); // categori
   
   $this->db->limit(5);

   $query = $this->db->get();
   return $query->result(); 

  
}

public function get_data_kategori(){
    $this->db->select('*');
   $this->db->from('kategori_v1');
    $query = $this->db->get()->result_array();
    
   //lakukan cek jumlah artikel dari setiap kategori, apabila kosong jangan tampilkan.
   for($i=0;$i<count($query);$i++)
   {
       $jumlahArtikel = $this->get_post_by_id_categori($query[$i]['id_categori_wp']);
       if($jumlahArtikel <= 0)
       {
           //jika jumlah artikel = 0 atau kurang, maka hilangkan array kategori tersebut
           unset($query[$i]); 
       }
       else
       {
           //jika jumlah artikal > 0, maka tambahkan indeks array baru "jumlah_artikel" ke array kategori
             $query[$i]['jumlah_artikel'] = $this->get_post_by_id_categori($query[$i]['id_categori_wp']);
       }   
       //indeks array kembali setelah unset
       $query = array_values($query);
     
   }
   return $query;
   }
   
   
public function debugCoratCoret()
{
     $this->db->select('*');
   $this->db->from('kategori');
    $query = $this->db->get()->result_array();
    
   //lakukan cek jumlah artikel dari setiap kategori, apabila kosong jangan tampilkan.
   for($i=0;$i<count($query);$i++)
   {
       $jumlahArtikel = $this->get_post_by_id_categori($query[$i]['id_categori_wp']);
       if($jumlahArtikel <= 0)
       {
           //jika jumlah artikel = 0 atau kurang, maka hilangkan array kategori tersebut
           unset($query[$i]); 
       }
       else
       {
           //jika jumlah artikal > 0, maka tambahkan indeks array baru "jumlah_artikel" ke array kategori
             $query[$i]['jumlah_artikel'] = $this->get_post_by_id_categori($query[$i]['id_categori_wp']);
       }   
       //indeks array kembali setelah unset
       $query = array_values($query);
     
   }
   return $query;
}
public function debugCoratCoret2()
{
    $this->db->select('*');
   $this->db->from('kategori');
    $query = $this->db->get()->result_array();
   //lakukan cek jumlah artikel dari setiap kategori, apabila kosong jangan tampilkan.
   for($i=0;$i<count($query);$i++)
   {
      $query[$i]['jumlah_artikel'] = $this->get_post_by_id_categori($query[$i]['id_categori_wp']);
   }
   return $query;
}


   public function get_post_by_date_fix2($totaldata, $halamanaktif){

      // rumus pagination

      $jumlahdataperhalaman=10;
      $jumlahhalaman=ceil($totaldata/$jumlahdataperhalaman);
      $awalData=($jumlahdataperhalaman*$halamanaktif)-$jumlahdataperhalaman;

      // end rumus


      $this->db->select('wp.ID, wp.post_author, wp.post_date, wp.post_title, wp.guid, wp.post_content, wpm2.meta_value, t2.display_name as user_login, c.name, c.term_id, d.gambar_user_wp');
      $this->db->from('wp_posts wp');
   
      $this->db->join('wp_postmeta wpm', 'wp.ID = wpm.post_id '); // get thumnile post
      $this->db->join('wp_postmeta wpm2', 'wpm.meta_value = wpm2.post_id'); // get thumnile post
      $this->db->join('wp_users t2', 't2.ID = wp.post_author'); // get name author
   
      $this->db->join('wp_term_relationships cr', 'wp.ID = cr.object_id'); // gcategori
      $this->db->join('wp_term_taxonomy ct', 'ct.term_taxonomy_id = cr.term_taxonomy_id'); // gcategori
      $this->db->join('wp_terms c', 'ct.term_id = c.term_id'); // gcategori

      $this->db->join('donatur d', 'wp.post_author = d.id_user_wp'); // gambar_autor
   
      $this->db->order_by('wp.post_date','DESC');
      $this->db->group_by('wp.ID');
   
      $this->db->where('wpm.meta_key','_thumbnail_id');
      $this->db->where('wpm2.meta_key','_wp_attached_file');
      $this->db->where('wp.post_status','publish');
      $this->db->where('wp.post_type','post');
   
      $this->db->where('ct.taxonomy','category'); // categori
      
      $this->db->limit($jumlahdataperhalaman, $awalData);
     // $this->db->limit(1);

      $query = $this->db->get();
      return $query->result(); 
   
     
   }

   public function getallpostBackup(){ //backup tanggal 29 oktober 2021
      $this->db->select('wp.ID, wp.post_author, wp.post_date, wp.post_title, wp.guid, wp.post_content, wpm2.meta_value, t2.display_name as user_login, c.name, c.term_id, d.gambar_user_wp');
      $this->db->from('wp_posts wp');
   
      $this->db->join('wp_postmeta wpm', 'wp.ID = wpm.post_id '); // get thumnile post
      $this->db->join('wp_postmeta wpm2', 'wpm.meta_value = wpm2.post_id'); // get thumnile post
      $this->db->join('wp_users t2', 't2.ID = wp.post_author'); // get name author
   
      $this->db->join('wp_term_relationships cr', 'wp.ID = cr.object_id'); // gcategori
      $this->db->join('wp_term_taxonomy ct', 'ct.term_taxonomy_id = cr.term_taxonomy_id'); // gcategori
      $this->db->join('wp_terms c', 'ct.term_id = c.term_id'); // gcategori

      $this->db->join('donatur d', 'wp.post_author = d.id_user_wp'); // gambar_autor
   
      $this->db->order_by('wp.post_date','DESC');
   
      $this->db->where('wpm.meta_key','_thumbnail_id');
      $this->db->where('wpm2.meta_key','_wp_attached_file');
      $this->db->where('wp.post_status','publish');
      $this->db->where('wp.post_type','post');
   
      $this->db->where('ct.taxonomy','category'); // categori
      
    //  $this->db->limit(20);
   
      $query = $this->db->get();
      return $query->num_rows(); 
   
     
   }
   
   public function getallpost(){
      $this->db->select('wp.ID');
      $this->db->from('wp_posts wp');
   
      $this->db->join('wp_postmeta wpm', 'wp.ID = wpm.post_id '); // get thumnile post
      $this->db->join('wp_postmeta wpm2', 'wpm.meta_value = wpm2.post_id'); // get thumnile post
      $this->db->join('wp_users t2', 't2.ID = wp.post_author'); // get name author
   
      $this->db->join('wp_term_relationships cr', 'wp.ID = cr.object_id'); // gcategori
      $this->db->join('wp_term_taxonomy ct', 'ct.term_taxonomy_id = cr.term_taxonomy_id'); // gcategori
      $this->db->join('wp_terms c', 'ct.term_id = c.term_id'); // gcategori

      $this->db->join('donatur d', 'wp.post_author = d.id_user_wp'); // gambar_autor
   
      $this->db->order_by('wp.post_date','DESC');
   
      $this->db->where('wpm.meta_key','_thumbnail_id');
      $this->db->where('wpm2.meta_key','_wp_attached_file');
      $this->db->where('wp.post_status','publish');
      $this->db->where('wp.post_type','post');
   
      $this->db->where('ct.taxonomy','category'); // categori
      
    //  $this->db->limit(20);
   
      $query = $this->db->get();
      return $query->num_rows(); 
   
     
   }

   public function search_fix($key){
      $this->db->select('wp.ID, wp.post_author, wp.post_date, wp.post_title, wp.guid, wp.post_content, wpm2.meta_value, t2.display_name as user_login, c.name, c.term_id, d.gambar_user_wp');
      $this->db->from('wp_posts wp');
   
      $this->db->join('wp_postmeta wpm', 'wp.ID = wpm.post_id '); // get thumnile post
      $this->db->join('wp_postmeta wpm2', 'wpm.meta_value = wpm2.post_id'); // get thumnile post
      $this->db->join('wp_users t2', 't2.ID = wp.post_author'); // get name author
   
      $this->db->join('wp_term_relationships cr', 'wp.ID = cr.object_id'); // gcategori
      $this->db->join('wp_term_taxonomy ct', 'ct.term_taxonomy_id = cr.term_taxonomy_id'); // gcategori
      $this->db->join('wp_terms c', 'ct.term_id = c.term_id'); // gcategori
   
      $this->db->join('donatur d', 'wp.post_author = d.id_user_wp'); // gambar_autor

      $this->db->order_by('wp.post_date','DESC');
   
      $this->db->where('wpm.meta_key','_thumbnail_id');
      $this->db->where('wpm2.meta_key','_wp_attached_file');
      $this->db->where('wp.post_status','publish');
      $this->db->where('wp.post_type','post');
   
      $this->db->where('ct.taxonomy','category'); // categori

      $this->db->like('wp.post_title', $query);
   
      $query = $this->db->get();
      return $query->num_rows(); 
   
     
   }

   public function westlist_user($id_user){
      $this->db->select('wp.ID, wp.post_author, wp.post_date, wp.post_title, wp.guid, wp.post_content, wpm2.meta_value, t2.display_name as user_login, c.name, c.term_id, uw.id_user_weslist, d.gambar_user_wp');
      $this->db->from('wpuf_posts wp');
   
      $this->db->join('wpuf_postmeta wpm', 'wp.ID = wpm.post_id '); // get thumnile post
      $this->db->join('wpuf_postmeta wpm2', 'wpm.meta_value = wpm2.post_id'); // get thumnile post
      $this->db->join('wpuf_users t2', 't2.ID = wp.post_author'); // get name author
   
      $this->db->join('wpuf_term_relationships cr', 'wp.ID = cr.object_id'); // gcategori
      $this->db->join('wpuf_term_taxonomy ct', 'ct.term_taxonomy_id = cr.term_taxonomy_id'); // gcategori
      $this->db->join('wpuf_terms c', 'ct.term_id = c.term_id'); // gcategori
   
      $this->db->join('donatur d', 'wp.post_author = d.id_user_wp'); // gambar_autor

      $this->db->join('user_weslist uw', 'uw.id_post = wp.ID'); // westlist dan post

      $this->db->order_by('uw.id_user_weslist','DESC');
   
      $this->db->where('wpm.meta_key','_thumbnail_id');
      $this->db->where('wpm2.meta_key','_wp_attached_file');
      $this->db->where('wp.post_status','publish');
      $this->db->where('wp.post_type','post');
   
      $this->db->where('ct.taxonomy','category'); // categori
      $this->db->where('uw.id_user',$id_user); // categori
      $query = $this->db->get();
      return $query->result(); 
   
     
   }

   public function search_westlist($data){

      $query= $this->db->get_where('user_weslist',$data);
      return $query->result_array();          
  }

  public function createWestlist($data){
   $this->db->insert('user_weslist', $data);
   return $this->db->affected_rows();
return $this->db->_error_message(); 
return $this->db->_error_number();
}


public function deletewestlist($id_postx){



   $this->db->where('id_user_weslist', $id_postx);
        return $this->db->delete('user_weslist');
   
}


public function notif_broadcase($tittle, $pesan, $image){

   $curl = curl_init();

   curl_setopt_array($curl, array(
     CURLOPT_URL => "https://fcm.googleapis.com/fcm/send",
     CURLOPT_RETURNTRANSFER => true,
     CURLOPT_ENCODING => "",
     CURLOPT_MAXREDIRS => 10,
     CURLOPT_TIMEOUT => 0,
     CURLOPT_FOLLOWLOCATION => true,
     CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
     CURLOPT_CUSTOMREQUEST => "POST",
     CURLOPT_POSTFIELDS =>'{
       "to":
       "/topics/zonainfo"
       ,
      "priority" : "high",
      "notification":{
               "title": "'.$tittle.'",
               "text": "'.$pesan.'",
               "image": "'.$image.'"         
           },
       "data": {
           "title": "'.$tittle.'",
           "text": "'.$pesan.'",
           "image": "'.$image.'"
           },
       }',
     CURLOPT_HTTPHEADER => array(
       "Content-type: application/json",
       "Authorization: key=AAAAJHRoVww:APA91bGXgI8mzP8BcZaz5pA4mi21RoDLJWBt5YLfrmVo7kBByvCOADnOAppOKD1LwOalERt4s6_Y8CGahJ8RMypRf0SRoSge93lkbUQAwk3RMwfrwTu6lysAfVfJUmcNAyQoqPFz2gzS",
       "Content-Type: application/json"
     ),
   ));
   
   $response = curl_exec($curl);
   
   curl_close($curl);
   return $response;

 }


 public function get_data_kategori_limit(){
   $this->db->select('*');
   $this->db->from('kategori');
   $this->db->limit(6);
   return $query = $this->db->get()->result_array();
   }


   public function get_author(){
      
      $this->db->select('u.ID, u.display_name, dn.gambar_user_wp');
      $this->db->from('wpuf_users u');
   
      $this->db->join('wpuf_usermeta m', 'u.ID = m.user_id'); // get thumnile post
      
      $this->db->join('donatur dn', 'u.ID = dn.id_user_wp'); // get img autor

      $this->db->like('m.meta_key','wpuf_capabilities');

      $query = $this->db->get();
      return $query->result(); 
      }


      public function get_post_by_id_author($id_author){
         $this->db->select('wp.ID, wp.post_author, wp.post_date, wp.post_title, wp.guid, wp.post_content, wpm2.meta_value, t2.display_name as user_login, c.name, c.term_id, d.gambar_user_wp');
         $this->db->from('wpuf_posts wp');
      
         $this->db->join('wpuf_postmeta wpm', 'wp.ID = wpm.post_id '); // get thumnile post
         $this->db->join('wpuf_postmeta wpm2', 'wpm.meta_value = wpm2.post_id'); // get thumnile post
         $this->db->join('wpuf_users t2', 't2.ID = wp.post_author'); // get name author
      
         $this->db->join('wpuf_term_relationships cr', 'wp.ID = cr.object_id'); // gcategori
         $this->db->join('wpuf_term_taxonomy ct', 'ct.term_taxonomy_id = cr.term_taxonomy_id'); // gcategori
         $this->db->join('wpuf_terms c', 'ct.term_id = c.term_id'); // gcategori
      
         $this->db->join('donatur d', 'wp.post_author = d.id_user_wp'); // gambar_autor
      
         $this->db->order_by('wp.post_date','DESC');
      
         $this->db->where('wpm.meta_key','_thumbnail_id');
         $this->db->where('wpm2.meta_key','_wp_attached_file');
         $this->db->where('wp.post_status','publish');
         $this->db->where('wp.post_type','post');
      
         $this->db->where('ct.taxonomy','category'); // categori
         $this->db->where('wp.post_author',$id_author); // categori
         $query = $this->db->get();
         return $query->result(); 
      
        
      }


public function get_versi(){
         $this->db->select('*');
         $this->db->from('apps_versi');
         return $query = $this->db->get()->result_array();
      }
      
      
      public function get_post_by_date_fix_limit_1($id_post){
         $this->db->select('wp.ID, wp.post_author, wp.post_date, wp.post_title, wp.guid, wp.post_content, wpm2.meta_value, t2.display_name as user_login, c.name, c.term_id, d.gambar_user_wp');
         $this->db->from('wp_posts wp');
      
         $this->db->join('wp_postmeta wpm', 'wp.ID = wpm.post_id '); // get thumnile post
         $this->db->join('wp_postmeta wpm2', 'wpm.meta_value = wpm2.post_id'); // get thumnile post
         $this->db->join('wp_users t2', 't2.ID = wp.post_author'); // get name author
      
         $this->db->join('wp_term_relationships cr', 'wp.ID = cr.object_id'); // gcategori
         $this->db->join('wp_term_taxonomy ct', 'ct.term_taxonomy_id = cr.term_taxonomy_id'); // gcategori
         $this->db->join('wp_terms c', 'ct.term_id = c.term_id'); // gcategori
      
         
         $this->db->join('donatur d', 'wp.post_author = d.id_user_wp'); // gambar_autor
      
         $this->db->order_by('wp.post_date','DESC');
      
         $this->db->where('wpm.meta_key','_thumbnail_id');
         $this->db->where('wpm2.meta_key','_wp_attached_file');
         $this->db->where('wp.post_status','publish');
         $this->db->where('wp.post_type','post');
      
         $this->db->where('ct.taxonomy','category'); // categori
         
         $this->db->where('wp.ID',$id_post);
         $this->db->limit(1);
      
         $query = $this->db->get();
         return $query->result_array(); 
      
        
      }
      
      public function getsavelike($data, $id_user){

         foreach ($data as $key => $value) {
            # code...
            $id_post=$value->ID;

            // var_dump($id_post);

            $this->db->select('*');
            $this->db->from('user_weslist');

            $this->db->where('id_user', $id_user );
            $this->db->where('id_post', $id_post );

            $query = $this->db->get();

            if ( $query->num_rows() > 0 )
            {
               $like="1";
            }else{
               $like="0";
            }

            $data_baru[] = array('ID'=> $value->ID, 'post_author'=> $value->post_author, 'post_date'=> $value->post_date,
            'post_title'=>$value->post_title, 'guid'=>$value->guid, 'post_content'=>$value->post_content, 'meta_value'=>$value->meta_value,
         'user_login'=>$value->user_login, 'name'=>$value->name, 'term_id'=>$value->term_id,'gambar_user_wp'=>$value->gambar_user_wp, 'like'=>$like);


         }

         return $data_baru;

      }

public function select_save($data){
         $this->db->select('*');
         $this->db->from('user_weslist');
         $this->db->where($data);

         return $query = $this->db->get()->result_array();
   }


 public function get_post_save($id_user){
      $this->db->select('wp.ID, wp.post_author, wp.post_date, wp.post_title, wp.guid, wp.post_content, wpm2.meta_value, t2.display_name as user_login, c.name, c.term_id, d.gambar_user_wp');
      $this->db->from('wp_posts wp');
   
      $this->db->join('wp_postmeta wpm', 'wp.ID = wpm.post_id '); // get thumnile post
      $this->db->join('wp_postmeta wpm2', 'wpm.meta_value = wpm2.post_id'); // get thumnile post
      $this->db->join('wp_users t2', 't2.ID = wp.post_author'); // get name author
   
      $this->db->join('wp_term_relationships cr', 'wp.ID = cr.object_id'); // gcategori
      $this->db->join('wp_term_taxonomy ct', 'ct.term_taxonomy_id = cr.term_taxonomy_id'); // gcategori
      $this->db->join('wp_terms c', 'ct.term_id = c.term_id'); // gcategori
   
      
      $this->db->join('donatur d', 'wp.post_author = d.id_user_wp'); // gambar_autor
   
      $this->db->join('user_weslist sv', 'wp.ID = sv.id_post'); // Save like

      $this->db->order_by('wp.post_date','DESC');
      
      $this->db->group_by('wp.ID');
   
      $this->db->where('wpm.meta_key','_thumbnail_id');
      $this->db->where('wpm2.meta_key','_wp_attached_file');
      $this->db->where('wp.post_status','publish');
      $this->db->where('wp.post_type','post');
   
      $this->db->where('ct.taxonomy','category'); // categori

      $this->db->where('sv.id_user',$id_user); // where id user save
   
      $query = $this->db->get();
      return $query->result_array(); 
   
     
   }


    public function dapatkanWaktuSekarang()
    {
        //load date helper
        $this->load->helper('date');

        $format = "%Y-%m-%d";
        return @mdate($format);
    }


   public function get_poling($date){

      $this->db->select('*');
      $this->db->from('Poling'); 
      
      $this->db->where('poling_start <=',$date);
     // $this->db->where('poling_end >=',$date);
      $this->db->where('status',"1");

      $query = $this->db->get();
      $queryResultArray =  $query->result_array(); 
      for($i=0;$i<count($queryResultArray);$i++)
      {
          $queryResultArray[$i]['waktu_sekarang'] = $this->dapatkanWaktuSekarang();
      }
      return $queryResultArray;

   }
   
   
   public function get_post_by_id_categori_pagination($id_categori, $totaldata, $halamanaktif){

       // rumus pagination

       $jumlahdataperhalaman=10;
       $jumlahhalaman=ceil($totaldata/$jumlahdataperhalaman);
       $awalData=($jumlahdataperhalaman*$halamanaktif)-$jumlahdataperhalaman;
 
       // end rumus


      $this->db->select('wp.ID, wp.post_author, wp.post_date, wp.post_title, wp.guid, wp.post_content, wpm2.meta_value, t2.display_name as user_login, c.name, c.term_id, d.gambar_user_wp');
      $this->db->from('wp_posts wp');
   
      $this->db->join('wp_postmeta wpm', 'wp.ID = wpm.post_id '); // get thumnile post
      $this->db->join('wp_postmeta wpm2', 'wpm.meta_value = wpm2.post_id'); // get thumnile post
      $this->db->join('wp_users t2', 't2.ID = wp.post_author'); // get name author
   
      $this->db->join('wp_term_relationships cr', 'wp.ID = cr.object_id'); // gcategori
      $this->db->join('wp_term_taxonomy ct', 'ct.term_taxonomy_id = cr.term_taxonomy_id'); // gcategori
      $this->db->join('wp_terms c', 'ct.term_id = c.term_id'); // gcategori
   
      $this->db->join('donatur d', 'wp.post_author = d.id_user_wp'); // gambar_autor
   
      $this->db->order_by('wp.post_date','DESC');
   
      $this->db->where('wpm.meta_key','_thumbnail_id');
      $this->db->where('wpm2.meta_key','_wp_attached_file');
      $this->db->where('wp.post_status','publish');
      $this->db->where('wp.post_type','post');
   
      $this->db->where('ct.taxonomy','category'); // categori
      $this->db->where('c.term_id',$id_categori); // categori

      $this->db->limit($jumlahdataperhalaman, $awalData);

      $query = $this->db->get();
      return $query->result(); 
   
     
   }
   
   public function search_fix_panigation($key, $total_post, $halamanaktif){

      // rumus pagination

      $jumlahdataperhalaman=10;
      $jumlahhalaman=ceil($totaldata/$jumlahdataperhalaman);
      $awalData=($jumlahdataperhalaman*$halamanaktif)-$jumlahdataperhalaman;

      // end rumus


      $this->db->select('wp.ID, wp.post_author, wp.post_date, wp.post_title, wp.guid, wp.post_content, wpm2.meta_value, t2.display_name as user_login, c.name, c.term_id, d.gambar_user_wp');
      $this->db->from('wp_posts wp');
   
      $this->db->join('wp_postmeta wpm', 'wp.ID = wpm.post_id '); // get thumnile post
      $this->db->join('wp_postmeta wpm2', 'wpm.meta_value = wpm2.post_id'); // get thumnile post
      $this->db->join('wp_users t2', 't2.ID = wp.post_author'); // get name author
   
      $this->db->join('wp_term_relationships cr', 'wp.ID = cr.object_id'); // gcategori
      $this->db->join('wp_term_taxonomy ct', 'ct.term_taxonomy_id = cr.term_taxonomy_id'); // gcategori
      $this->db->join('wp_terms c', 'ct.term_id = c.term_id'); // gcategori
   
      $this->db->join('donatur d', 'wp.post_author = d.id_user_wp'); // gambar_autor

      $this->db->order_by('wp.post_date','DESC');
      $this->db->group_by('wp.ID');
   
      $this->db->where('wpm.meta_key','_thumbnail_id');
      $this->db->where('wpm2.meta_key','_wp_attached_file');
      $this->db->where('wp.post_status','publish');
      $this->db->where('wp.post_type','post');
   
      $this->db->where('ct.taxonomy','category'); // categori

      $this->db->like('wp.post_title', $key);
   
      $this->db->limit($jumlahdataperhalaman, $awalData);

      $query = $this->db->get();
      return $query->result(); 
   
     
   }




   public function get_view_post($id_user, $id_post, $date){

      $this->db->select('*');
      $this->db->from('custom_post_view');

      $this->db->where('tgl_view',$date);
      $this->db->where('id_user',$id_user);
      $this->db->where('id_post',$id_post);

      $query = $this->db->get();
      return $query->result_array(); 
   }

   public function insert_post_view($data){
      $this->db->insert('custom_post_view', $data);
      return $this->db->affected_rows();
      return $this->db->_error_message(); 
      return $this->db->_error_number();

   }

   public function update_post_total_view($id_post){
      $this->db->select('*');
      $this->db->from('custom_total_post_view');
      $this->db->where('id_post',$id_post);

      $query = $this->db->get()->result_array();

      if($query){
         // update

            foreach ($query as $key) {
              $total= $key['total_view'];;
            }

            $data=[
               'total_view'=>$total+1
   
               ]; 
         
         $this->db->where('id_post', $id_post);
         return $this->db->update('custom_total_post_view',$data);

      }else{
         // insert
         $data=[
            'id_post'=>$id_post,
            'total_view'=>'1'

            ];  

         $this->db->insert('custom_total_post_view', $data);
         return $this->db->affected_rows();
         return $this->db->_error_message(); 
         return $this->db->_error_number();
      }
   }
   
   
   
    public function getallpost_trending(){ 

      $maktu_minim=date('Y-m-d', time() - (60 * 60 * 24 * 7)) ;


      $this->db->select('wp.ID');
      $this->db->from('wp_posts wp');
   
      $this->db->join('wp_postmeta wpm', 'wp.ID = wpm.post_id '); // get thumnile post
      $this->db->join('wp_postmeta wpm2', 'wpm.meta_value = wpm2.post_id'); // get thumnile post
      $this->db->join('wp_users t2', 't2.ID = wp.post_author'); // get name author
   
      $this->db->join('wp_term_relationships cr', 'wp.ID = cr.object_id'); // gcategori
      $this->db->join('wp_term_taxonomy ct', 'ct.term_taxonomy_id = cr.term_taxonomy_id'); // gcategori
      $this->db->join('wp_terms c', 'ct.term_id = c.term_id'); // gcategori

      $this->db->join('donatur d', 'wp.post_author = d.id_user_wp'); // gambar_autor

      $this->db->join('custom_total_post_view t_view', 'wp.ID = t_view.id_post'); // total view
   
      $this->db->order_by('t_view.total_view','DESC');
   
      //$this->db->order_by('t_view.tgl_publis <=',$maktu_minim);
      //$this->db->where('wp.post_date <=',$maktu_minim);
      
      $this->db->where('wp.post_date >=',$maktu_minim);

      $this->db->where('wpm.meta_key','_thumbnail_id');
      $this->db->where('wpm2.meta_key','_wp_attached_file');
      $this->db->where('wp.post_status','publish');
      $this->db->where('wp.post_type','post');
   
      $this->db->where('ct.taxonomy','category'); // categori
      
      //      $this->db->limit(20);
   
      $query = $this->db->get();
      return $query->num_rows(); 
   
     
   }

    public function getallpost_trendingBackup(){ //backup tanggal 29 oktober 2021

      $maktu_minim=date('Y-m-d', time() - (60 * 60 * 24 * 7)) ;


      $this->db->select('wp.ID, wp.post_author, wp.post_date, wp.post_title, wp.guid, wp.post_content, wpm2.meta_value, t2.display_name as user_login, c.name, c.term_id, d.gambar_user_wp, t_view.total_view');
      $this->db->from('wp_posts wp');
   
      $this->db->join('wp_postmeta wpm', 'wp.ID = wpm.post_id '); // get thumnile post
      $this->db->join('wp_postmeta wpm2', 'wpm.meta_value = wpm2.post_id'); // get thumnile post
      $this->db->join('wp_users t2', 't2.ID = wp.post_author'); // get name author
   
      $this->db->join('wp_term_relationships cr', 'wp.ID = cr.object_id'); // gcategori
      $this->db->join('wp_term_taxonomy ct', 'ct.term_taxonomy_id = cr.term_taxonomy_id'); // gcategori
      $this->db->join('wp_terms c', 'ct.term_id = c.term_id'); // gcategori

      $this->db->join('donatur d', 'wp.post_author = d.id_user_wp'); // gambar_autor

      $this->db->join('custom_total_post_view t_view', 'wp.ID = t_view.id_post'); // total view
   
      $this->db->order_by('t_view.total_view','DESC');
   
      //$this->db->order_by('t_view.tgl_publis <=',$maktu_minim);
      //$this->db->where('wp.post_date <=',$maktu_minim);
      
      $this->db->where('wp.post_date >=',$maktu_minim);

      $this->db->where('wpm.meta_key','_thumbnail_id');
      $this->db->where('wpm2.meta_key','_wp_attached_file');
      $this->db->where('wp.post_status','publish');
      $this->db->where('wp.post_type','post');
   
      $this->db->where('ct.taxonomy','category'); // categori
      
      //      $this->db->limit(20);
   
      $query = $this->db->get();
      return $query->num_rows(); 
   
     
   }

   public function get_post_by_date_fix2_trending($totaldata, $halamanaktif){

      $maktu_minim=date('Y-m-d', time() - (60 * 60 * 24 * 7)) ;
      // rumus pagination

      $jumlahdataperhalaman=10;
      $jumlahhalaman=ceil($totaldata/$jumlahdataperhalaman);
      $awalData=($jumlahdataperhalaman*$halamanaktif)-$jumlahdataperhalaman;

      // end rumus

      $this->db->select('wp.ID, wp.post_author, wp.post_date, wp.post_title, wp.guid, wp.post_content, wpm2.meta_value, t2.display_name as user_login, c.name, c.term_id, d.gambar_user_wp, t_view.total_view');
      $this->db->from('wp_posts wp');
   
      $this->db->join('wp_postmeta wpm', 'wp.ID = wpm.post_id '); // get thumnile post
      $this->db->join('wp_postmeta wpm2', 'wpm.meta_value = wpm2.post_id'); // get thumnile post
      $this->db->join('wp_users t2', 't2.ID = wp.post_author'); // get name author
   
      $this->db->join('wp_term_relationships cr', 'wp.ID = cr.object_id'); // gcategori
      $this->db->join('wp_term_taxonomy ct', 'ct.term_taxonomy_id = cr.term_taxonomy_id'); // gcategori
      $this->db->join('wp_terms c', 'ct.term_id = c.term_id'); // gcategori

      $this->db->join('donatur d', 'wp.post_author = d.id_user_wp'); // gambar_autor
   
      $this->db->join('custom_total_post_view t_view', 'wp.ID = t_view.id_post'); // total view

      $this->db->order_by('t_view.total_view','DESC');
      
      $this->db->group_by('wp.ID');
   
      // $this->db->order_by('t_view.total_view','DESC');

      $this->db->where('wp.post_date >=',$maktu_minim);

      $this->db->where('wpm.meta_key','_thumbnail_id');
      $this->db->where('wpm2.meta_key','_wp_attached_file');
      $this->db->where('wp.post_status','publish');
      $this->db->where('wp.post_type','post');
   
      $this->db->where('ct.taxonomy','category'); // categori
      
      $this->db->limit($jumlahdataperhalaman, $awalData);
   
      $query = $this->db->get();
      return $query->result(); 
   
     
   }

  public function get_notifikasi(){

      $this->db->select('*');
      $this->db->from('custom_notifikasi'); 
      $this->db->order_by('custom_notifikasi.id_notifikasi','DESC');
      $query = $this->db->get();
      return $query->result_array(); 


   }
   
   
   public function rekomendasi_post($id_categori, $id_post){


     $this->db->select('wp.ID, wp.post_author, wp.post_date, wp.post_title, wp.guid, wp.post_content, wpm2.meta_value, t2.display_name as user_login, c.name, c.term_id, d.gambar_user_wp');
     $this->db->from('wp_posts wp');
  
     $this->db->join('wp_postmeta wpm', 'wp.ID = wpm.post_id '); // get thumnile post
     $this->db->join('wp_postmeta wpm2', 'wpm.meta_value = wpm2.post_id'); // get thumnile post
     $this->db->join('wp_users t2', 't2.ID = wp.post_author'); // get name author
  
     $this->db->join('wp_term_relationships cr', 'wp.ID = cr.object_id'); // gcategori
     $this->db->join('wp_term_taxonomy ct', 'ct.term_taxonomy_id = cr.term_taxonomy_id'); // gcategori
     $this->db->join('wp_terms c', 'ct.term_id = c.term_id'); // gcategori
  
     $this->db->join('donatur d', 'wp.post_author = d.id_user_wp'); // gambar_autor
  
     $this->db->order_by('wp.post_date','DESC');

     $this->db->where('wp.ID !=',$id_post);

     $this->db->where('wp.post_status','publish');
  
     $this->db->where('wpm.meta_key','_thumbnail_id');
     $this->db->where('wpm2.meta_key','_wp_attached_file');
     $this->db->where('wp.post_status','publish');
     $this->db->where('wp.post_type','post');
  
     $this->db->where('ct.taxonomy','category'); // categori
     $this->db->where('c.term_id',$id_categori); // categori

     $this->db->limit(10);

     $query = $this->db->get();
     return $query->result(); 
  
    
  }
  
  public function get_detail_poling($id_poling){

   $this->db->select('dp.*, p.poling_end');
    $this->db->from('Detail_poling dp'); 
   $this->db->join('Poling p','dp.id_poling = p.id_poling');
   $this->db->where('dp.id_poling',$id_poling);
   $query = $this->db->get();
   return $query->result_array(); 


}
  public function get_detail_poling_backup($id_poling){ //backup sebelum perubahan redaksi halaman grafik polling ketika polling selesai tgl 27 oktober 2021

   $this->db->select('*');
   $this->db->from('Detail_poling'); 
   $this->db->where('id_poling',$id_poling);
   $query = $this->db->get();
   return $query->result_array(); 


}




public function save_poling($data){
   $this->db->insert('Histori_poling', $data);
   return $this->db->affected_rows();
return $this->db->_error_message(); 
return $this->db->_error_number();
}



public function getsavelike_trending($data, $id_user){

   foreach ($data as $key => $value) {
      # code...
      $id_post=$value->ID;

      // var_dump($id_post);

      $this->db->select('*');
      $this->db->from('user_weslist');

      $this->db->where('id_user', $id_user );
      $this->db->where('id_post', $id_post );

      $query = $this->db->get();

      if ( $query->num_rows() > 0 )
      {
         $like="1";
      }else{
         $like="0";
      }

      $data_baru[] = array('ID'=> $value->ID, 'post_author'=> $value->post_author, 'post_date'=> $value->post_date,
      'post_title'=>$value->post_title, 'guid'=>$value->guid, 'post_content'=>$value->post_content, 'meta_value'=>$value->meta_value,
   'user_login'=>$value->user_login, 'name'=>$value->name, 'term_id'=>$value->term_id,'gambar_user_wp'=>$value->gambar_user_wp, 'like'=>$like, 'total_view'=>''.$value->total_view);


   }

   return $data_baru;

}



public function get_komen($id_post){
   
 $this->db->select('a.komentar,a.id_user,a.id_komentar,(DATE_ADD(a.created_at,INTERVAL 7 HOUR)) AS waktu_komentar, b.person_photo, b.person_name');
  
   $this->db->from('Custom_komentar a');
   $this->db->join('user b', 'b.person_id=a.id_user');
   $this->db->order_by('a.created_at','DESC');

   $this->db->where('a.id_post',$id_post);
   return $query = $this->db->get()->result_array();
}


public function total_komentar($id_post){
   $this->db->select('*');
   $this->db->from('Custom_komentar');
   $this->db->where('Custom_komentar.id_post',$id_post);
   return $query = $this->db->get()->num_rows();
}



public function insert_komen($data){
   $this->db->insert('Custom_komentar', $data);
   return $this->db->affected_rows();
   return $this->db->_error_message(); 
   return $this->db->_error_number();

}


public function createFile($data){
   $this->db->insert('Custom_file', $data);
   return $this->db->affected_rows();
return $this->db->_error_message(); 
return $this->db->_error_number();
}



public function cek_poling($id_user){

   $this->db->select('*');
   $this->db->from('Histori_poling');
   $this->db->where('id_user', $id_user );

   return $query = $this->db->get()->result_array();

}




public function get_file_ok(){
   $this->db->select('*');
   $this->db->from('Download_file');
   $this->db->order_by('Download_file.id_download','DESC');
   return $query = $this->db->get()->result_array();
}

  

public function cek_poling_fix($id_user, $id_poling){

   $this->db->select('*');
   $this->db->from('Histori_poling');
   $this->db->where('id_user', $id_user );
   $this->db->where('id_poling', $id_poling );
   return $query = $this->db->get()->result_array();

}


public function get_post_by_id($id_post){
   $this->db->select('wp.ID, wp.post_author, wp.post_date, wp.post_title, wp.guid, wp.post_content, wpm2.meta_value, t2.display_name as user_login, c.name, c.term_id, d.gambar_user_wp');
   $this->db->from('wp_posts wp');

   $this->db->join('wp_postmeta wpm', 'wp.ID = wpm.post_id '); // get thumnile post
   $this->db->join('wp_postmeta wpm2', 'wpm.meta_value = wpm2.post_id'); // get thumnile post
   $this->db->join('wp_users t2', 't2.ID = wp.post_author'); // get name author

   $this->db->join('wp_term_relationships cr', 'wp.ID = cr.object_id'); // gcategori
   $this->db->join('wp_term_taxonomy ct', 'ct.term_taxonomy_id = cr.term_taxonomy_id'); // gcategori
   $this->db->join('wp_terms c', 'ct.term_id = c.term_id'); // gcategori

   $this->db->join('donatur d', 'wp.post_author = d.id_user_wp'); // gambar_autor

   $this->db->order_by('wp.post_date','DESC');

   $this->db->where('wpm.meta_key','_thumbnail_id');
   $this->db->where('wpm2.meta_key','_wp_attached_file');
   $this->db->where('wp.post_status','publish');
   $this->db->where('wp.post_type','post');
   
   $this->db->where('ct.taxonomy','category'); // categori

   $this->db->limit(1);

   $this->db->where('wp.ID',$id_post); // id post
   $query = $this->db->get();
   return $query->result_array(); 

  
}

public function deletewestlist_ok($data){

   $this->db->where($data);
     return $this->db->delete('user_weslist');

}

public function save_notifikasi($data){
   $this->db->insert('custom_notifikasi', $data);
   return $this->db->affected_rows();
   return $this->db->_error_message(); 
   return $this->db->_error_number();

}

public function setMenyukaiOrDeleteMenyukaiIfExist($id_user,$id_post)
{
    //cek apakah id_user dan id_posting ada. apabila ada hapus, apabila tidak ada tambahkan baru
    $this->db->select('*');
      $this->db->from('like_artikel');

      $this->db->where('id_user', $id_user );
      $this->db->where('id_post', $id_post );
      $result = "";
      $query = $this->db->get();

      if ( $query->num_rows() > 0 ) 
      {
         $result =  $this->prosesHapusMenyukai($id_user,$id_post);
          //jika sudah pernah like. maka hapus like itu
        
      }else{
         $result = $this->prosesTambahMenyukai($id_user,$id_post);
          //jika tidak ada like. maka tambahkan like
         
      }
      return $result;

}

private function prosesHapusMenyukai($id_user,$id_post)
{
    $pesanRespon='';
   $data = [
       'id_user' => $id_user,
       'id_post' => $id_post
       ];
   $rowAffected =  $this->db->delete('like_artikel',$data);
   if($rowAffected >= 0)
   {
       $pesanRespon = 2;
   }
   else
   {
       $pesanRespon = 0;
   }
   return $pesanRespon;
}

private function prosesTambahMenyukai($id_user,$id_post)
{
    $pesanRespon='';
    $data = [
        'id_user' => $id_user,
        'id_post' => $id_post
        ];
    $rowAffected = $this->db->insert('like_artikel',$data);
    if($rowAffected >= 0)
    {
       $pesanRespon = 1;
       }
    else
    {
       $pesanRespon = 0;
    }
    return $pesanRespon;
    return $this->db->_error_message(); 
    return $this->db->_error_number();
}


public function getLikeTotalLikeTotalComment($id_user, $id_post)
    {
        $resultLike = $this->getLike($id_user, $id_post);
        $resultAllLike = $this->getAllLike($id_post);
        $resultAllComment = $this->total_komentar($id_post);
        $data = [
            'resultLike' => $resultLike,
            'resultAllLike' => $resultAllLike,
            'resultAllComment' => $resultAllComment
            ];
        return $data;    
    }

public function getLike($id_user, $id_post)
{
        //cek apakah id_user dan id_posting ada. apabila ada hapus, apabila tidak ada tambahkan baru
        $this->db->select('*');
        $this->db->from('like_artikel');

        $this->db->where('id_user', $id_user );
        $this->db->where('id_post', $id_post );
        $result = 0;
        $query = $this->db->get();

        if ( $query->num_rows() > 0 ) 
        {
         $result = 1; //1 maka ada like
        }else{
         $result = 0; //0 maka tidak ada like
        }
        return $result; 
     
}

public function getAllLike($id_post)
{
        $this->db->select('*');
        $this->db->from('like_artikel');
        $this->db->where('id_post', $id_post );
        $query = $this->db->get();
        $result = $query->num_rows();
        return $result; 
}


// Cek notif
public function cek_custom_notif($id_reverensi){

   $this->db->select('*');
   $this->db->from('custom_notifikasi');
   $this->db->where('id_reverensi', $id_reverensi );

   return $query = $this->db->get()->result_array();

}

public function delete_komentar($data)
{
       $this->db->where($data);
     return $this->db->delete('Custom_komentar');
}

public function get_nomor_wa($nama)
{
    $this->db->select('*');
    $this->db->from('nomor_wa_admin_zona');
    $this->db->where('nama', $nama);
    
    return $query = $this->db->get()->row_array(); //return 1 row saja
    
}

public function getSemuaPenggunaYangLikeDanCekLikeKomentarPenggunaSekarangByIdKomentar($id_komentar,$person_id)
{
         $this->db->select('person_id'); //seleksi hanya person id saja yg meng like suatu id_komentar
         $this->db->from('custom_like_komentar');
         $this->db->where('id_custom_komentar', $id_komentar);
         $query = $this->db->get()->result_array();
         $adaLikeDariUserSekarang = false;

         //cek apakah ada like dari current user terhadap salah satu dari id_komentar
         for($i=0;$i<count($query);$i++)
         {
             if($query[$i]['person_id'] == $person_id){
                $adaLikeDariUserSekarang = true;
                break;
             }
             else{
                 $adaLikeDariUserSekarang = false;
             }
         }
         $respon = [
             'person_id' => $query,
             'ada_like' => $adaLikeDariUserSekarang
         ];
         return $respon; 
}

public function setLikeKomentarOrDeleteLikeKomentarIfExist($id_custom_komentar,$person_id)
{
    //cek apakah id_person dan id_komentar ada. apabila ada hapus, apabila tidak ada tambahkan baru
      $this->db->select('*');
      $this->db->from('custom_like_komentar');

      $this->db->where('person_id', $person_id );
      $this->db->where('id_custom_komentar', $id_custom_komentar );
      $result = 0;
      $query = $this->db->get();

      if ( $query->num_rows() > 0 ) 
      {
         $result =  $this->prosesHapusLikeKomentar($id_custom_komentar,$person_id);
          //jika sudah pernah like komentar. maka hapus like komentar itu
        
      }else{
         $result = $this->prosesTambahLikeKomentar($id_custom_komentar,$person_id);
          //jika tidak ada like komentar. maka tambahkan like komentar
         
      }
      return $result;
}


private function prosesHapusLikeKomentar($id_custom_komentar,$person_id){
    $berhasilHapus=0;
    $data = [
        'id_custom_komentar' => $id_custom_komentar,
        'person_id' => $person_id
        ];
    $rowAffected =  $this->db->delete('custom_like_komentar',$data);
    if($rowAffected >= 0)
    {
        $berhasilHapus = 2;
    }
    else
    {
        $berhasilHapus = 0;
    }
    return $berhasilHapus;
}


private function prosesTambahLikeKomentar($id_custom_komentar,$person_id)
{
    $berhasilTambah=0;
    $data = [
        'id_custom_komentar' => $id_custom_komentar,
        'person_id' => $person_id
        ];
    $rowAffected = $this->db->insert('custom_like_komentar',$data);
    if($rowAffected >= 0)
    {
       $berhasilTambah = 1;
    }
    else
    {
       $berhasilTambah = 0;
    }
    return $berhasilTambah;
    return $this->db->_error_message(); 
    return $this->db->_error_number();
}



    public function coba_insert_balas_komentar($data){
        $query = $this->db->insert('Custom_komentar', $data);
         return $this->db->affected_rows();
         return $this->db->_error_message(); 
         return $this->db->_error_number();
    }
    
     public function insert_balas_komentar($data){
        $query = $this->db->insert('Custom_komentar', $data);
         return $this->db->affected_rows();
         return $this->db->_error_message(); 
         return $this->db->_error_number();
    }


    public function get_label_detail_poling($id_poling)
    {
        $this->db->select('keterangan_untuk_textview');
        $this->db->from('Poling');
        $this->db->where('id_poling', $id_poling);
    
         $query = $this->db->get()->row_array(); //return 1 row saja
        if($query)
        {
            return $query;
        }
        else{
            return "";
        }
    }

    public function getSubKomentarAndLike($id_komentar)
    {
     
        $this->db->select('csk.id_sub_komentar, csk.id_komentar, csk.komentar, csk.created_at, csk.id_user');
        $this->db->from('custom_sub_komentar csk');
  
        $this->db->join('Custom_komentar ck','csk.id_komentar = ck.id_komentar'); 
  
        
  
        $this->db->where('csk.id_komentar',$id_komentar);  

        $query = $this->db->get();
        $hasilAwal = $query->result_array();
        
        for($i=0;$i<count($hasilAwal);$i++)
        {
            //start penambahan like
            $hasilAwal[$i]['like'] = $this->getJumlahLikeSubKomentar($hasilAwal[$i]['id_sub_komentar']);
            //end penambahan like    
            

            //start dapatkan apakah ada like dari current user
            $hasilAwal[$i]['ada_like_current_user'] = $this->All_model->getCurrentUserLikeOnSubKomentar($hasilAwal[$i]['id_sub_komentar'],$hasilAwal[$i]['id_user']);
            //end dapatkan apakah ada like dari current user
        }
        $hasilAkhir = $hasilAwal;
       
        
        return $hasilAkhir;
    }
    
    public function getJumlahLikeSubKomentar($id_sub_komentar)
    {
        $this->db->select('*');
        $this->db->from('custom_like_sub_komentar');
        $this->db->where('id_custom_sub_komentar',$id_sub_komentar);
        
        $query = $this->db->get();
        return $query->num_rows(); 
        
    }


public function getCurrentUserLikeOnSubKomentar($id_sub_komentar,$person_id)
{
         $this->db->select('person_id'); //seleksi hanya person id saja yg meng like suatu id_sub_komentar
         $this->db->from('custom_like_sub_komentar');
         $this->db->where('id_custom_sub_komentar', $id_sub_komentar);
         $query = $this->db->get()->result_array();
         $adaLikeDariUserSekarang = false;

         //cek apakah ada like dari current user terhadap salah satu dari id_sub_komentar
         for($i=0;$i<count($query);$i++)
         {
             if($query[$i]['person_id'] == $person_id){
                $adaLikeDariUserSekarang = true;
                break;
             }
             else{
                 $adaLikeDariUserSekarang = false;
             }
         }
        return $adaLikeDariUserSekarang;
}


public function insert_sub_komen($data){
   $this->db->insert('custom_sub_komentar', $data);
   return $this->db->affected_rows();
   return $this->db->_error_message(); 
   return $this->db->_error_number();
}



    

public function setLikeSubKomentarOrDeleteLikeSubKomentarIfExist($id_custom_sub_komentar, $person_id)
{
    //cek apakah id_person dan id_komentar ada. apabila ada hapus, apabila tidak ada tambahkan baru
      $this->db->select('*');
      $this->db->from('custom_like_sub_komentar');

      $this->db->where('person_id', $person_id );
      $this->db->where('id_custom_sub_komentar', $id_custom_sub_komentar );
      $result = 0;
      $query = $this->db->get();

      if ( $query->num_rows() > 0 ) 
      {
         $result =  $this->prosesHapusLikeSubKomentar($id_custom_sub_komentar,$person_id);
          //jika sudah pernah like komentar. maka hapus like komentar itu
        
      }else{
         $result = $this->prosesTambahLikeSubKomentar($id_custom_sub_komentar,$person_id);
          //jika tidak ada like komentar. maka tambahkan like komentar
         
      }
      return $result;
}



private function prosesHapusLikeSubKomentar($id_custom_sub_komentar,$person_id){
    $berhasilHapus=0;
    $data = [
        'id_custom_sub_komentar' => $id_custom_sub_komentar,
        'person_id' => $person_id
        ];
    $rowAffected =  $this->db->delete('custom_like_sub_komentar',$data);
    if($rowAffected >= 0)
    {
        $berhasilHapus = 2;
    }
    else
    {
        $berhasilHapus = 0;
    }
    return $berhasilHapus;
}


private function prosesTambahLikeSubKomentar($id_custom_sub_komentar,$person_id)
{
    $berhasilTambah=0;
    $data = [
        'id_custom_sub_komentar' => $id_custom_sub_komentar,
        'person_id' => $person_id
        ];
    $rowAffected = $this->db->insert('custom_like_sub_komentar',$data);
    if($rowAffected > 0)
    {
       $berhasilTambah = 1;
    }
    else
    {
       $berhasilTambah = 0;
    }
    return $berhasilTambah;
    return $this->db->_error_message(); 
    return $this->db->_error_number();
}


public function delete_sub_komentar($data)
{
       $this->db->where($data);
       $this->db->delete('custom_sub_komentar');
     return $this->db->affected_rows();
}

public function get_alasan_poling($id_poling,$id_user)
{
         $this->db->select('alasan'); //seleksi hanya alasan berdasarkan id polling dan id user
         $this->db->from('Histori_poling');
         $this->db->where('id_poling', $id_poling);
         $this->db->where('id_user', $id_user);
         $query = $this->db->get()->result_array();
         return $query;
}



public function get_gambar_viewpager($jumlahItemViewPager)
{
    $trending = $this->get_viewpager_gambar_poling_trending($jumlahItemViewPager);
    
    if($trending == null)
    {
        //jika trending = null, maka tampilkan gambar poling saja berjumlah 3 secara normal
        $trending = $this->get_viewpager_gambar_poling_normal($jumlahItemViewPager);
    }
    for($i=0;$i<count($trending);$i++)
    {
        $trending[$i]['waktu_sekarang'] = $this->dapatkanWaktuSekarang();
    }
   
    return $trending;
}


public function get_viewpager_gambar_poling_trending($jumlahItemViewPager)
{
    
    //ambil 3 polling yang memiliki jumlah vote paling banyak
    $sql = "SELECT hp.id_poling, COUNT(hp.id_poling) AS 'jumlah_vote',p.img_poling, p.tiitle_poling , p.poling_start, p.poling_end FROM Histori_poling hp INNER JOIN Poling p ON hp.id_poling = p.id_poling WHERE status = '1' GROUP BY id_poling ORDER BY jumlah_vote  DESC LIMIT $jumlahItemViewPager";
    $query = $this->db->query($sql);
    return $query->result_array();
}

public function get_viewpager_gambar_poling_normal($jumlahItemViewPager)
{
   
    //ambil 3 polling terakhir by id
    $sql = "SELECT id_poling, 0 AS 'jumlah_vote', img_poling, tiitle_poling, poling_start, poling_end FROM Poling WHERE status = '1' ORDER BY id_poling DESC  LIMIT $jumlahItemViewPager";
    $query = $this->db->query($sql);
    return $query->result_array();
}

public function testboy($id_poling)
{
    $hasil = "Dari Test Boy ".$id_poling." okay boy?";
    return $hasil;
}

public function get_data_notifikasi_upload_karya($id_user)
{
        $sql = "SELECT cf.id_file, cf.url_file, u.person_name, u.person_email FROM Custom_file cf INNER JOIN user u ON cf.id_user = u.person_id WHERE cf.id_user = $id_user ORDER BY cf.id_file  DESC LIMIT 1";
    $query = $this->db->query($sql);
    $hasil = $query->result_array();
   return $hasil;
}


public function get_simpan_by_idpost($data)
{
    $this->db->select('*');
    $this->db->from('user_weslist');
    $this->db->where($data);
    $query = $this->db->get()->result_array();
    return $query;
    
}


 public function getsavelikesatuartikel($data, $id_user){


            $id_post=$data[0]['ID'];


            $this->db->select('*');
            $this->db->from('user_weslist');

            $this->db->where('id_user', $id_user );
            $this->db->where('id_post', $id_post );

            $query = $this->db->get();

            if ( $query->num_rows() > 0 )
            {
               $like="1";
            }else{
               $like="0";
            }

        //     $data_baru[] = array('ID'=> $value->ID, 'post_author'=> $value->post_author, 'post_date'=> $value->post_date,
        //     'post_title'=>$value->post_title, 'guid'=>$value->guid, 'post_content'=>$value->post_content, 'meta_value'=>$value->meta_value,
        //  'user_login'=>$value->user_login, 'name'=>$value->name, 'term_id'=>$value->term_id,'gambar_user_wp'=>$value->gambar_user_wp, 'like'=>$like);

            
                $data[0]['like'] = $like;
         
                
         return $data;

      }
      
function getGambarPemenang($pilihan_yang_menang){
        $this->db->select('img_tokoh');
    $this->db->from('Detail_poling');
    $this->db->where('nama_tokoh',$pilihan_yang_menang);
    return $this->db->get()->result_array();
    
}

function getStatusNotifUpdate()
{
    //  $this->db->select('status_popup_update');
    // $this->db->from('popup_update');
    // $this->db->limit(1);
    return $this->db->get('popup_update')->row()->status_popup_update;
}



//==========================================START DEVELOP SECTION===================================================

public function get_file_ok_develop(){
   $this->db->select('*');
   $this->db->from('Download_file_develop');
   $this->db->order_by('Download_file_develop.id_download','DESC');
   return $query = $this->db->get()->result_array();
}


 public function get_post_by_date_fix2_develop($totaldata, $halamanaktif){

      // rumus pagination

      $jumlahdataperhalaman=10;
      $jumlahhalaman=ceil($totaldata/$jumlahdataperhalaman);
      $awalData=($jumlahdataperhalaman*$halamanaktif)-$jumlahdataperhalaman;

      // end rumus


      $this->db->select('wp.ID, wp.post_author, wp.post_date, wp.post_title, wp.guid, wp.post_content, wpm2.meta_value, t2.display_name as user_login, c.name, c.term_id, d.gambar_user_wp');
      $this->db->from('wp_posts wp');
   
      $this->db->join('wp_postmeta wpm', 'wp.ID = wpm.post_id'); // get thumnile post
      $this->db->join('wp_postmeta wpm2', 'wpm.meta_value = wpm2.post_id'); // get thumnile post
      $this->db->join('wp_users t2', 't2.ID = wp.post_author'); // get name author
   
      $this->db->join('wp_term_relationships cr', 'wp.ID = cr.object_id'); // gcategori
      $this->db->join('wp_term_taxonomy ct', 'ct.term_taxonomy_id = cr.term_taxonomy_id'); // gcategori
      $this->db->join('wp_terms c', 'ct.term_id = c.term_id'); // gcategori

      $this->db->join('donatur d', 'wp.post_author = d.id_user_wp'); // gambar_autor
   
      $this->db->order_by('wp.post_date','DESC');
      $this->db->group_by('wp.ID');
   
      $this->db->where('wpm.meta_key','_thumbnail_id');
      $this->db->where('wpm2.meta_key','_wp_attached_file');
      $this->db->where('wp.post_status','publish');
      $this->db->where('wp.post_type','post');
   
      $this->db->where('ct.taxonomy','category'); // categori
      
      $this->db->limit($jumlahdataperhalaman, $awalData);
     // $this->db->limit(1);

      $query = $this->db->get();
      return $query->result(); 
   
     
   }
   
   
   public function get_post_by_date_fix2_dengan_waktu($totaldata, $halamanaktif){

      // rumus pagination

      $jumlahdataperhalaman=10;
      $jumlahhalaman=ceil($totaldata/$jumlahdataperhalaman);
      $awalData=($jumlahdataperhalaman*$halamanaktif)-$jumlahdataperhalaman;

      // end rumus


      $this->db->select('wp.ID, wp.post_author, wp.post_date, wp.post_title, wp.guid, wp.post_content, wpm2.meta_value, t2.display_name as user_login, c.name, c.term_id, d.gambar_user_wp');
      $this->db->from('wp_posts wp');
   
      $this->db->join('wp_postmeta wpm', 'wp.ID = wpm.post_id '); // get thumnile post
      $this->db->join('wp_postmeta wpm2', 'wpm.meta_value = wpm2.post_id'); // get thumnile post
      $this->db->join('wp_users t2', 't2.ID = wp.post_author'); // get name author
   
      $this->db->join('wp_term_relationships cr', 'wp.ID = cr.object_id'); // gcategori
      $this->db->join('wp_term_taxonomy ct', 'ct.term_taxonomy_id = cr.term_taxonomy_id'); // gcategori
      $this->db->join('wp_terms c', 'ct.term_id = c.term_id'); // gcategori

      $this->db->join('donatur d', 'wp.post_author = d.id_user_wp'); // gambar_autor
   
      $this->db->order_by('wp.post_date','DESC');
      $this->db->group_by('wp.ID');
   
      $this->db->where('wpm.meta_key','_thumbnail_id');
      $this->db->where('wpm2.meta_key','_wp_attached_file');
      $this->db->where('wp.post_status','publish');
      $this->db->where('wp.post_type','post');
   
      $this->db->where('ct.taxonomy','category'); // categori
      
      $this->db->limit($jumlahdataperhalaman, $awalData);
     // $this->db->limit(1);

      $query = $this->db->get();
      return $query->result(); 
   
     
   }
   
   public function formatDatePost($data)
   {
        foreach ($data as $key => $value) {
            # code...
            $post_date=$value->post_date;
                
           
           

            $data_baru[] = array('ID'=> $value->ID, 'post_author'=> $value->post_author, 'post_date'=> $post_date,
            'post_title'=>$value->post_title, 'guid'=>$value->guid, 'post_content'=>$value->post_content, 'meta_value'=>$value->meta_value,
         'user_login'=>$value->user_login, 'name'=>$value->name, 'term_id'=>$value->term_id,'gambar_user_wp'=>$value->gambar_user_wp);


         }

         return $data;
   }

//=============================================END DEVELOP SECTION===================================================

}



?>