<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Email_notif_upload_karya extends CI_Controller {


	public function __construct()
{
    parent::__construct();
    $this->load->model('All_model');
}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
	   
        
		echo "controller kirim email";
		//$this->load->view('coba_kirim_email');
        $this->send_mail();

	}
	
	public function send_mail(){
	    ini_set( 'display_errors', 1 );
error_reporting( E_ALL );
$from = "it-developer@zonamahasiswa.id";
$to = "fandyjoansyah@gmail.com";
$subject = "Checking PHP mail";
$message = "PHP mail works just fine";
$headers = "From:" . $from;
mail($to,$subject,$message, $headers);
echo "The email message was sent.";  
	}
	
	public function send_mail_ci($user_id){
	    $this->load->library('email');

        //start query model dapatkan data upload karya
        $hasil = $this->All_model->get_data_notifikasi_upload_karya($user_id);
        //end query model dapatkan data upload karya

        //start initiating email
        $this->email->set_newline("\r\n");
        $this->email->from('it-developer@zonamahasiswa.id');
        $this->email->to('fandyjoansyah@gmail.com');
        $this->email->subject('Karya Terbaru Rubrik Sobat Zona');
        $this->email->message("Hai! ada karya terbaru yang baru saja diupload oleh pengguna dari apps zona mahasiswa \r\n
         Nama Pengirim: ".$hasil[0]['person_name']." 
         \r\n Email Pengirim: ".$hasil[0]['person_email']);
       $urlFile = $hasil[0]['url_file'];
       //mendapatkan semua text (nama file) setelah karakter "uploadedFiles/"
       $namaFile = substr($urlFile, strpos($urlFile, "uploadedFiles/") + 14);
	   $fileForEmail = $_SERVER["DOCUMENT_ROOT"]."/uploadedFiles/".$namaFile;
        $this->email->attach($fileForEmail); 
        //end initiating email
        

        //start kirim email
          if ($this->email->send()) {
            echo '<b>Your</b> Email has successfully been sent.';
        } else {
            show_error($this->email->print_debugger());
        }
        //end kirim email
    
	}
	
	public function cek_direktori()
	{
	    $namaFile = '2021-09-25-02:41:41.doc';
	    $filenya = FCPATH.'uploadedFiles/'.$namaFile;
	    return $filenya;
	}
	
	public function get_file()
	{
	   $namaFile = "2021-09-25-02:41:41.doc";
	   $fileForEmail = $_SERVER["DOCUMENT_ROOT"]."/uploadedFiles/".$namaFile;
	   echo $fileForEmail;
	}
	
	public function cek_koneksi_model()
	{
	    
	    $user_id="103641696102986163134";
	    
	    $hasil = $this->All_model->get_data_notifikasi_upload_karya($user_id);
	    print_r($hasil);
	}
	
	public function rapikan_email()
	{
	    $this->load->library('email');
        //start initiating email
        $this->email->set_newline("\r\n");
        $this->email->set_mailtype("html");
        $this->email->from('rubriksobatzona@zonamahasiswa.id', 'Rubrik Sobat Zona');
        $this->email->to('fandyjoansyah@gmail.com');
        $this->email->subject('Karya Terbaru Rubrik Sobat Zona');
        $this->email->message("test rubah display name");
        //end initiating email
        
        
         //start kirim email
          if ($this->email->send()) {
            echo '<b>Your</b> Email has successfully been sent.';
        } else {
            show_error($this->email->print_debugger());
        }
        //end kirim email
	}
	
	public function coba_kirim_via_gmail()
	{
	     $config = [
            'mailtype'  => 'html',
            'charset'   => 'utf-8',
            'protocol'  => 'smtp',
            'smtp_host' => 'smtp.gmail.com',
            'smtp_user' => 'rubriksobatzona@zonamahasiswa.id',  // Email gmail
            'smtp_pass'   => 'emailZona5758',  // Password gmail
            'smtp_crypto' => 'ssl',
            'smtp_port'   => 465,
            'crlf'    => "\r\n",
            'newline' => "\r\n"
        ];
        
         // Load library email dan konfigurasinya
        $this->load->library('email', $config);
        
        // Email dan nama pengirim
        $this->email->from('rubriksobatzona@zonamahasiswa.id');

        // Email penerima
        $this->email->to('samfandxev@gmail.com'); // Ganti dengan email tujuan
        
        $this->email->message('pesan oke');
        
        $this->email->message('subjek oke');
        
        
         //start kirim email
          if ($this->email->send()) {
            echo '<b>Your</b> Email has successfully been sent.';
        } else {
            show_error($this->email->print_debugger());
        }
        //end kirim email
	}

}
