<?php

defined('BASEPATH') or exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Video_comment_last extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        // $this->load->model('All_model');
        $this->load->model('Video_model');
    }

    public function index_post()
    {
        $id_user = $this->input->post('id_user') != null ? $this->input->post('id_user') : null;
        $video_id = $this->input->post('id_video') != null ? $this->input->post('id_video') : null;
        $id_comment = $this->input->post('id_comment') != null ? $this->input->post('id_comment') : null;
        $video_comment = $this->Video_model->video_comment_last($id_user, $video_id, $id_comment);
        // print_r($video_comment);
        $countComment = $this->Video_model->video_comment_count(null, $video_id);
        // $countComment = $this->Video_model->video_comment_count_level_one($video_id);
        if ($countComment != 0) {
            $totalComment = count($video_comment);
            for ($i = 0; $i < $totalComment; $i++) {
                $video_comment[$i]['convert_time'] = $this->convertTime($video_comment[$i]['video_comment_date_created']);
            }
        } else {
            $video_comment = null;
        }
        if ($video_comment || $video_comment == null) {
            // $lastItem = ($totalComment - 1) == 0 ? 0 : $video_comment[($totalComment - 1)]['video_comment_id'];
            $this->response([
                'status' => true,
                'message' => 'OK',
                'video_comment' => $countComment,
                'comment' => $video_comment,
            ], REST_Controller::HTTP_OK);
        } else {

            $this->response([
                'status' => false,
                'message' => 'Tidak ada komentar',
            ], REST_Controller::HTTP_OK);

        }

    }

    public function convertTime($datetime)
    {
        $time = strtotime($datetime);
        $diff = time() - $time;
        $diff /= 60;
        $var1 = floor($diff);
        $var = $var1 <= 1 ? 'menit' : 'menit';
        if ($diff >= 60) {
            $diff /= 60;
            $var1 = floor($diff);
            $var = $var1 <= 1 ? 'jam' : 'jam';
            if ($diff >= 24) {$diff /= 24;
                $var1 = floor($diff);
                $var = $var1 <= 1 ? 'hari' : 'hari';
                if ($diff >= 30.4375) {$diff /= 30.4375;
                    $var1 = floor($diff);
                    $var = $var1 <= 1 ? 'bulan' : 'bulan';
                    if ($diff >= 12) {$diff /= 12;
                        $var1 = floor($diff);
                        $var = $var1 <= 1 ? 'tahun' : 'tahun';}
                }
            }
        }
        return $var1 . ' ' . $var . ' yang lalu';
    }

}