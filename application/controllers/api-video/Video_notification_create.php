<?php

defined('BASEPATH') or exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Video_notification_create extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        // $this->load->model('All_model');
        $this->load->model('Video_model');
    }

    public function index_post()
    {
        $id_user = $this->input->post('id_user') != null ? $this->input->post('id_user') : null;
        $id_notification = $this->input->post('id_notification') != null ? $this->input->post('id_notification') : null;
        if ($id_user == null || $id_notification == null) {
            $this->response([
                'status' => false,
                'message' => 'Data tidak valid',
            ], REST_Controller::HTTP_OK);
            exit();
        }
        $data = [
            'video_notification_id' => $id_notification,
            'user_id' => $id_user,
            'video_notification_date' => date('Y-m-d H:i:s'),
        ];
        $cekRead = $this->Video_model->video_notification_read($id_user, $id_notification);
        if (count($cekRead) == 0) {
            $video_notification_store = $this->Video_model->video_notification_store($data);
        } else {
            $video_notification_store = true;
        }
        // print_r($video_notification);
        if ($video_notification_store) {

            $this->response([
                'status' => true,
                'message' => 'OK',
                'data' => $data,
            ], REST_Controller::HTTP_OK);
        } else {

            $this->response([
                'status' => false,
                'message' => 'GAGAL',
            ], REST_Controller::HTTP_OK);
        }
    }
}