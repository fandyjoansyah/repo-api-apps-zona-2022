<?php

defined('BASEPATH') or exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Video_save_create extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        // $this->load->model('All_model');
        $this->load->model('Video_model');
    }

    public function index_post()
    {
        $id_user = $this->input->post('id_user') != null ? $this->input->post('id_user') : null;
        $video_id = $this->input->post('id_video') != null ? $this->input->post('id_video') : null;
        $save = $this->input->post('save') != null ? $this->input->post('save') : null;
        if ($id_user == null || $video_id == null || $save == null) {
            $this->response([
                'status' => false,
                'message' => 'Data tidak valid',
            ], REST_Controller::HTTP_OK);
            exit();
        }

        $data = [
            'video_id' => $video_id,
            'user_id' => $id_user,
        ];
        $videoSave = $this->Video_model->video_save_count($id_user, $video_id);
        if ($save == 1) {
            if ($videoSave == 0) {
                $video_save = $this->Video_model->video_save_store($data);
            }
        } else {
            if ($videoSave >= 1) {
                $video_save = $this->Video_model->video_save_delete($data);
            }
        }
        $video_share_count = $this->Video_model->video_save_count(null, $video_id);
        $datasave = [
            'video_save' => $video_share_count,
        ];
        $video_update = $this->Video_model->video_update($video_id, $datasave);
        if ($video_update) {

            $this->response([
                'status' => true,
                'message' => 'OK',
                'data' => $data,
            ], REST_Controller::HTTP_OK);

        } else {

            $this->response([
                'status' => false,
                'message' => 'Gagal menambah komentar',
            ], REST_Controller::HTTP_OK);

        }

    }

}