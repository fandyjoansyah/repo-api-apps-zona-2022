<?php

defined('BASEPATH') or exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Video_get_count extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        // $this->load->model('All_model');
        $this->load->model('Video_model');
    }

    public function index_post()
    {
        $id_user = $this->input->post('id_user') != null ? $this->input->post('id_user') : null;
        $id_video = $this->input->post('id_video') != null ? $this->input->post('id_video') : null;
        if ($id_user == null) {
            $this->response([
                'status' => false,
                'message' => 'Data tidak valid',
            ], REST_Controller::HTTP_OK);
            exit();
        }
        
        $count_video_comment = $this->Video_model->video_comment_count_level_one($id_video);
        $count_video_like = $this->Video_model->video_like_count('', $id_video);
        $count_video_like_user = $this->Video_model->video_like_count($id_user, $id_video);
        $count_video_share = $this->Video_model->video_share_count($id_user, $id_video);
        $count_video_save = $this->Video_model->video_save_count('', $id_video);
        $count_video_save_user = $this->Video_model->video_save_count($id_user, $id_video);
        $count_video_view = $this->Video_model->video_view_count('', $id_video);
        
        $data = [
            'video_comment' => $count_video_comment,
            'video_like' => $count_video_like,
            'video_share' => $count_video_share,
            'video_save' => $count_video_save,
            'video_view' => $count_video_view,
            'user_like' => $count_video_like_user,
            'user_save' => $count_video_save_user,
            
        ];
        if ($video_notification || $video_notification == 0) {
            $this->response([
                'status' => true,
                'message' => 'OK',
                'data' => $data,
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => false,
                'message' => 'GAGAL',
            ], REST_Controller::HTTP_OK);
        }
    }

}