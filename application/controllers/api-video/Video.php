<?php

defined('BASEPATH') or exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Video extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        // $this->load->model('All_model');
        $this->load->model('Video_model');
    }

    public function index_post()
    {
        $id_user = $this->input->post('id_user') != null ? $this->input->post('id_user') : null;
        $video_id = $this->input->post('id_video') != null ? $this->input->post('id_video') : null;
        $category_id = $this->input->post('video_category_id') != null ? $this->input->post('video_category_id') : null;
        $category_sub_id = $this->input->post('video_category_sub_id') != null ? $this->input->post('video_category_sub_id') : null;
        $next_page = $this->input->post('halamanaktif') != null ? $this->input->post('halamanaktif') : null;
        if ($category_id == 1 || empty($category_id) && empty($category_sub_id)) {
            $video = $this->Video_model->video_popular($id_user, $video_id, $category_id, $category_sub_id, $next_page);
            if ($video) {
                $lastItem = $next_page + 1;
            }
        } else {
            $video = $this->Video_model->video($id_user, $video_id, $category_id, $category_sub_id, $next_page);
        }
        // print_r($video);
        $totalVideo = count($video);
        for ($i = 0; $i < $totalVideo; $i++) {
            $video[$i]['video_share_url'] = 'https://share.zonamahasiswa.id/?id=' . $video[$i]['video_id'] . '&shareid=' . $id_user;
            $video[$i]['convert_time'] = $this->convertTime($video[$i]['video_date_created']);
            $comment = $this->Video_model->video_comment($id_user, $video[$i]['video_id'], null, null);
            $video[$i]['comment'] = [null];
            // $video[$i]['video_comment'] = $this->Video_model->video_comment_count_level_one($video_id);

            if (count($comment) != 0) {
                $video[$i]['comment'] = [
                    0 => $comment[0],
                ];
                $video[$i]['comment'][0]['convert_time'] = $comment[0] != null ? $this->convertTime($comment[0]['video_comment_date_created']) : null;
                // $video[$i]['comment'][1]['convert_time'] = $comment[1] != null ? $this->convertTime($comment[1]['video_comment_date_created']) : null;
            }
        }
        if ($video) {
            $lastItem = ($totalVideo - 1) == 0 ? 0 : $video[($totalVideo - 1)]['video_id'];
            $this->response([
                'status' => true,
                'message' => 'OK',
                'next_item' => $lastItem,
                'data' => $video,
            ], REST_Controller::HTTP_OK);

        } else {

            $this->response([
                'status' => false,
                'message' => 'GAGAL',
            ], REST_Controller::HTTP_OK);

        }

    }

    public function convertTime($datetime)
    {
        $time = strtotime($datetime);
        $diff = time() - $time;
        $diff /= 60;
        $var1 = floor($diff);
        $var = $var1 <= 1 ? 'menit' : 'menit';
        if ($diff >= 60) {
            $diff /= 60;
            $var1 = floor($diff);
            $var = $var1 <= 1 ? 'jam' : 'jam';
            if ($diff >= 24) {$diff /= 24;
                $var1 = floor($diff);
                $var = $var1 <= 1 ? 'hari' : 'hari';
                if ($diff >= 30.4375) {$diff /= 30.4375;
                    $var1 = floor($diff);
                    $var = $var1 <= 1 ? 'bulan' : 'bulan';
                    if ($diff >= 12) {$diff /= 12;
                        $var1 = floor($diff);
                        $var = $var1 <= 1 ? 'tahun' : 'tahun';}
                }
            }
        }
        return $var1 . ' ' . $var . ' yang lalu';
    }

}