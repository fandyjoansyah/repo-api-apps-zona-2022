<?php

defined('BASEPATH') or exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Video_like extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        // $this->load->model('All_model');
        $this->load->model('Video_model');
    }

    public function index_post()
    {
        $id_user = $this->input->post('id_user') != null ? $this->input->post('id_user') : null;
        $video_id = $this->input->post('id_video') != null ? $this->input->post('id_video') : null;
        $like = $this->input->post('like') != null ? $this->input->post('like') : null;
        if ($id_user == null || $video_id == null || $like == null) {
            $this->response([
                'status' => false,
                'message' => 'Data tidak valid',
            ], REST_Controller::HTTP_OK);
            exit();
        }
        //PERHITUNGAN DAN INPUT NOTIFIKASI VIDEO
        $getVideo = $this->Video_model->video("", $video_id, "", "", "");
        $date = date('Y-m-d H:i:s');
        if ($getVideo[0]['user_id'] != null) {
            $cekNotif = $this->Video_model->video_notification_count($getVideo[0]['user_id'], $video_id, "like")[0]['total'];
            if ($cekNotif == 0) {
                $dataNotif = [
                    'video_id' => $video_id,
                    'person_id' => $getVideo[0]['user_id'],
                    'from_person_id' => $id_user,
                    'video_notification_title' => "Menyukai video anda",
                    'video_notification_description' => '',
                    'video_notification_image' => $getVideo[0]['video_thumbnail'],
                    'video_notification_type' => 'like',
                    'video_notification_date' => $date,
                ];
                $this->Video_model->video_notification_store($dataNotif);
            }

            // count rate video
            $count = ($getVideo[0]['video_like'] + $getVideo[0]['video_view'] + $getVideo[0]['video_comment']) / 3;
            $cek_rate = $this->Video_model->video_rate($video_id, $date);
            // print_r($cek_rate);
            if (count($cek_rate) == 0) {
                $datarate = [
                    'video_id' => $video_id,
                    'video_rate_count' => $count,
                    'video_rate_date' => $date,
                ];
                $this->Video_model->video_rate_store($datarate);
            } else {
                $datarate = [
                    'video_rate_count' => $count,
                ];
                $this->Video_model->video_rate_update($video_id, $date, $datarate);
            }
        }
        $hitungSemuaRate = $this->Video_model->video_rate_count($video_id);

        $data = [
            'video_id' => $video_id,
            'user_id' => $id_user,
        ];
        $videoLike = $this->Video_model->video_like($data);
        if ($like == 1) {
            if ($this->Video_model->video_like_count($id_user, $video_id) == 0) {
                if ($videoLike[0]->total == 0) {
                    $video_like = $this->Video_model->video_like_store($data);
                }
            }
        } else {
            if ($this->Video_model->video_like_count($id_user, $video_id) > 0) {
                if ($videoLike[0]->total >= 1) {
                    $video_like = $this->Video_model->video_like_delete($data);
                }
            }
        }

        $video_like_count = $this->Video_model->video_like_count(null, $video_id);
        // print_r($hitungSemuaRate);
        $datasave = [
            'video_like' => $video_like_count,
            'video_rate' => $hitungSemuaRate,
        ];
        $video_share_update = $this->Video_model->video_update($video_id, $datasave);
        // print_r($video_share_update);
        // print_r($video_comment_store);
        if ($video_share_update) {

            $this->response([
                'status' => true,
                'message' => 'OK',
                'data' => $data,
            ], REST_Controller::HTTP_OK);

        } else {

            $this->response([
                'status' => false,
                'message' => 'Gagal menambah komentar',
            ], REST_Controller::HTTP_OK);

        }

    }

}