<?php

defined('BASEPATH') or exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Video_comment_like extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        // $this->load->model('All_model');
        $this->load->model('Video_model');
    }

    public function index_post()
    {
        $id_user = $this->input->post('id_user') != null ? $this->input->post('id_user') : null;
        $video_comment_id = $this->input->post('id_comment') != null ? $this->input->post('id_comment') : null;
        $like = $this->input->post('like') != null ? $this->input->post('like') : null;
        $user_id_liked = $this->input->post('user_id_liked') != null ? $this->input->post('user_id_liked') : null;
        $video_id = $this->input->post('id_video') != null ? $this->input->post('id_video') : null;
        if ($id_user == null || $video_comment_id == null || $like == null) {
            $this->response([
                'status' => false,
                'message' => 'Data tidak valid',
            ], REST_Controller::HTTP_OK);
            exit();
        }
        $getVideo = $this->Video_model->video("", $video_id, "", "", "");
        $getComment = $this->Video_model->video_comment_by_id($video_comment_id);
        $data = [
            'video_comment_id' => $video_comment_id,
            'user_id' => $id_user,
        ];
        $videoLike = $this->Video_model->video_comment_like($data);
        if ($like == 1) {
            if ($this->Video_model->video_comment_like_count($id_user, $video_comment_id) == 0) {
                if ($videoLike[0]->total == 0) {
                    $video_comment_like = $this->Video_model->video_comment_like_store($data);
                }
            }
        } else {
            if ($this->Video_model->video_comment_like_count($id_user, $video_comment_id) > 0) {
                if ($videoLike[0]->total >= 1) {
                    $video_comment_like = $this->Video_model->video_comment_like_delete($data);
                }
            }
        }
        // print_r($getComment['0']['video_id']);
        $cekNotif = $this->Video_model->video_notification_count_liked($user_id_liked, $getComment[0]['video_id'], "like", $video_comment_id)[0]['total'];
        // print_r($cekNotif);
            if ($cekNotif == 0) {
                $dataNotif = [
                    'video_id' => $getComment['0']['video_id'],
                    'person_id' => $user_id_liked == null ? $getComment[0]['user_id'] : $user_id_liked,
                    'from_person_id' => $id_user,
                    'video_notification_title' => "menyukai komentar anda",
                    'video_notification_description' => $comment,
                    'video_notification_image' => $getVideo[0]['video_thumbnail'],
                    'video_notification_type' => 'like',
                    'video_notification_date' => date('Y-m-d H:i:s'),
                    'comment_id' => $video_comment_id,
                ];
                $this->Video_model->video_notification_store($dataNotif);
            }
        $video_like_count = $this->Video_model->video_comment_like_count(null, $video_comment_id);
        $datasave = [
            'video_comment_like' => $video_like_count,
        ];
        $video_share_update = $this->Video_model->video_comment_update($video_comment_id, $datasave);
        // print_r($video_comment_store);
        if ($video_share_update) {

            $this->response([
                'status' => true,
                'message' => 'OK',
                'data' => $data,
            ], REST_Controller::HTTP_OK);

        } else {

            $this->response([
                'status' => false,
                'message' => 'Gagal menambah komentar',
            ], REST_Controller::HTTP_OK);

        }

    }

}