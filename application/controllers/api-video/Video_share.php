<?php

defined('BASEPATH') or exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Video_share extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        // $this->load->model('All_model');
        $this->load->model('Video_model');
    }

    public function index_post()
    {
        $id_user = $this->input->post('id_user') != null ? $this->input->post('id_user') : null;
        $video_id = $this->input->post('id_video') != null ? addslashes($this->input->post('id_video')) : null;
        $share_id = $this->input->post('id_share') != null ? addslashes($this->input->post('id_share')) : null;
        if ($id_user == null || $video_id == null) {
            $this->response([
                'status' => false,
                'message' => 'Data tidak valid',
            ], REST_Controller::HTTP_OK);
            exit();
        }
        // $video = $this->Video_model->video($id_user, $video_id, null, null);
        if ($this->Video_model->video_share_count($share_id, $video_id) == 0) {
            $data = [
                'video_id' => $video_id,
                'user_id' => $share_id,
            ];
            $this->Video_model->video_share_store($data);
        }
        $video_share_count = $this->Video_model->video_share_count(null, $video_id);
        $datasave = [
            'video_share' => $video_share_count,
        ];
        $video_share_update = $this->Video_model->video_update($video_id, $datasave);
        if ($video_share_update) {

            $this->response([
                'status' => true,
                'message' => 'OK',
                'data' => $data,
                'count_share' => $video_share_count,
            ], REST_Controller::HTTP_OK);

        } else {

            $this->response([
                'status' => false,
                'message' => 'Gagal menambah komentar',
            ], REST_Controller::HTTP_OK);

        }

    }

}