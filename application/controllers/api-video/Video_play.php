<?php

defined('BASEPATH') or exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Video_play extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('All_model');
        $this->load->model('Video_model');
        $this->load->library('user_agent');
    }

    public function index_post()
    {
        $id_user = $this->input->post('id_user') != null ? $this->input->post('id_user') : null;
        $video_id = $this->input->post('id_video') != null ? $this->input->post('id_video') : null;
        $position = $this->input->post('position') != null ? $this->input->post('position') : null;
        $miliseconds = $this->input->post('miliseconds') != null ? $this->input->post('miliseconds') : null;
        if ($id_user == null || $video_id == null || $position == null || $miliseconds == null) {
            $this->response([
                'status' => false,
                'message' => 'Data tidak valid',
            ], REST_Controller::HTTP_OK);
            exit();
        }

$useragent = $_SERVER['HTTP_USER_AGENT'];
        $arrAgent = [];
        preg_match('/Android (\d+)/i', $useragent, $arrAgent);
        $str_version = $arrAgent[0] ? $arrAgent[0] : 'Unknown OS';
        $ip = $this->input->ip_address();
        $date=date("Y-m-d");
        
        
        //ambil device name
        if ($this->agent->is_browser()){
                $device = $this->agent->browser().' '.$this->agent->version();
        }elseif ($this->agent->is_robot()){
                $device = $this->agent->robot();
        }elseif ($this->agent->is_mobile()){
                $device = $this->agent->mobile();
        }else{
                $device = 'Unidentified User Agent';
        }
        
        
        //cek ip di database
        $cekIp = $this->All_model->ip_address($ip, $date);
        if(count($cekIp) == 0){
            $result_curl = $this->curl_get('http://ip-api.com/json/'.$ip);
            $arrayIp = json_decode($result_curl, true);
            $dataIp = [
                'ip' => $ip,
                'country' => $arrayIp['country'],
                'address' => $arrayIp['region'] . ' ' . $arrayIp['regionName'] . ' ' . $arrayIp['city'] . ' ' . $arrayIp['zip'],
                'isp' => $arrayIp['isp'],
                'json' => $result_curl,
                'date' => $date,
                ];
            $this->All_model->ip_address_store($dataIp);
        }


        $data = [
            'video_id' => $video_id,
            'user_id' => $id_user,
        ];
        $cek_video_play = $this->Video_model->video_play($data);
        if (!$cek_video_play) {
            $data_play = [
                'video_id' => $video_id,
                'user_id' => $id_user,
                'video_play_position' => $position,
                'video_play_total' => $miliseconds,
            ];
            $this->Video_model->video_play_store($data_play);
        } else {
            if ($position <= $cek_video_play[0]->video_play_position) {
                $data_play = [
                    'video_play_position' => $position,
                ];
            } else {
                $total_menonton = $cek_video_play[0]->video_play_total + $miliseconds;
                $persentase_play = ($total_menonton / $cek_video_play[0]->video_duration) * 100;
                if ($persentase_play >= 50) {
                    $cek_video_view = $this->Video_model->video_view_count($id_user, $video_id);
                    
                    $data['device'] = $device;
                    $data['ip'] = $ip;
                    $data['os'] = $str_version;
                    $data['user_agent'] = $useragent;
                    if (!$cek_video_view) {
                        $this->Video_model->video_view_store($data);
                    }
                }
                $data_play = [
                    'video_play_position' => $position,
                    'video_play_total' => $total_menonton,
                ];
            }
            $this->Video_model->video_play_update($data_play, $data);
        }

        $video_duration_count = $this->Video_model->video_duration_count(null, $video_id);
        $video_view_count = $this->Video_model->video_view_count(null, $video_id);
        $datasave = [
            'video_view' => $video_view_count,
            'video_play_total' => $video_duration_count,
        ];
        $video_update = $this->Video_model->video_update($video_id, $datasave);
        if ($video_update) {

            $this->response([
                'status' => true,
                'message' => 'OK',
                'data' => $data,
            ], REST_Controller::HTTP_OK);

        } else {

            $this->response([
                'status' => false,
                'message' => 'Gagal menambah komentar',
            ], REST_Controller::HTTP_OK);

        }

    }

    public function curl_get($url){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        $output = curl_exec($ch); 
        return $output;
    }
    
}