<?php

defined('BASEPATH') or exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Video_notification_count extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        // $this->load->model('All_model');
        $this->load->model('Video_model');
    }

    public function index_post()
    {
        $id_user = $this->input->post('id_user') != null ? $this->input->post('id_user') : null;
        if ($id_user == null) {
            $this->response([
                'status' => false,
                'message' => 'Data tidak valid',
            ], REST_Controller::HTTP_OK);
            exit();
        }
        $getNotifOpen = $this->Video_model->video_open_notif($id_user);
        if (count($getNotifOpen) > 0) {
            $video_notification = $this->Video_model->video_notif_count($id_user, $getNotifOpen[0]['video_open_notif_date']);
        } else {
            $data = [
                'user_id' => $id_user,
                'video_open_notif_date' => date('Y-m-d H:i:s'),
            ];
            $this->Video_model->video_open_notif_store($data);
            $video_notification = 0;
        }
        if ($video_notification || $video_notification == 0) {
            $this->response([
                'status' => true,
                'message' => 'OK',
                'data' => $video_notification,
            ], REST_Controller::HTTP_OK);
        } else {

            $this->response([
                'status' => false,
                'message' => 'GAGAL',
            ], REST_Controller::HTTP_OK);
        }
    }

}