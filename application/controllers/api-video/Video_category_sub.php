<?php

defined('BASEPATH') or exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Video_category_sub extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        // $this->load->model('All_model');
        $this->load->model('Video_model');
    }

    public function index_post()
    {
        $id_user = $this->input->post('id_user');
        $category_id = $this->input->post('video_category_id');
        if ($id_user == null || $category_id == null) {
            $this->response([
                'status' => false,
                'message' => 'Data tidak valid',
            ], REST_Controller::HTTP_OK);
            exit();
        }
        $video_category_sub = $this->Video_model->video_category_sub($category_id);
        if ($video_category_sub) {

            $this->response([
                'status' => true,
                'message' => 'OK',
                'data' => $video_category_sub,
            ], REST_Controller::HTTP_OK);

        } else {

            $this->response([
                'status' => false,
                'message' => 'GAGAL',
                'data' => null,
            ], REST_Controller::HTTP_OK);

        }

    }

}