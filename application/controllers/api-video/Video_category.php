<?php

defined('BASEPATH') or exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Video_category extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        // $this->load->model('All_model');
        $this->load->model('Video_model');
    }

    public function index_post()
    {
        $video_category = $this->Video_model->video_category();
        for ($i = 0; $i < count($video_category); $i++) {
            $subCategory = $this->Video_model->video_category_sub($video_category[$i]['video_category_id']);
            $video_category[$i]['sub_category'] = count($subCategory) > 0 ? $subCategory : null;
        }
        if ($video_category) {

            $this->response([
                'status' => true,
                'message' => 'OK',
                'data' => $video_category,
            ], REST_Controller::HTTP_OK);

        } else {

            $this->response([
                'status' => false,
                'message' => 'GAGAL',
            ], REST_Controller::HTTP_OK);

        }

    }

}