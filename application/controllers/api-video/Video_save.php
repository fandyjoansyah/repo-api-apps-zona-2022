<?php

defined('BASEPATH') or exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Video_save extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        // $this->load->model('All_model');
        $this->load->model('Video_model');
    }

    public function index_post()
    {
        $id_user = $this->input->post('id_user') != null ? $this->input->post('id_user') : null;
        if ($id_user == null) {
            $this->response([
                'status' => false,
                'message' => 'Data tidak valid',
            ], REST_Controller::HTTP_OK);
            exit();
        }

        $data = [
            'user_id' => $id_user,
        ];
        $videoSave = $this->Video_model->video_save($data);
        // print_r($videoSave);
        if ($videoSave) {

            $this->response([
                'status' => true,
                'message' => 'OK',
                'data' => $videoSave,
            ], REST_Controller::HTTP_OK);

        } else {

            $this->response([
                'status' => false,
                'message' => 'Gagal menambah komentar',
            ], REST_Controller::HTTP_OK);

        }

    }

}