<?php

defined('BASEPATH') or exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Video_comment_reply_create extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        // $this->load->model('All_model');
        $this->load->model('Video_model');
    }

    public function index_post()
    {
        $id_user = $this->input->post('id_user') != null ? $this->input->post('id_user') : null;
        $video_id = $this->input->post('id_video') != null ? $this->input->post('id_video') : null;
        $comment = $this->input->post('comment') != null ? addslashes($this->input->post('comment')) : null;
        $id_user_reply = $this->input->post('id_user_reply') != null ? $this->input->post('id_user_reply') : null;
        $id_comment = $this->input->post('id_comment') != null ? $this->input->post('id_comment') : null;

        if ($id_user == null || $video_id == null || $comment == null || $id_comment == null || $id_user_reply == null) {
            $this->response([
                'status' => false,
                'message' => 'Data tidak valid',
            ], REST_Controller::HTTP_OK);
            exit();
        }

        $getVideo = $this->Video_model->video("", $video_id, "", "", "");
        
     $date = date('Y-m-d H:i:s');
        $data = [
            'video_comment_reply_id' => $id_comment,
            'video_comment_status' => 'reply',
            'video_comment' => $comment,
            'video_id' => $video_id,
            'user_id' => $id_user,
            'user_id_reply' => $id_user_reply,
            'video_comment_date_created' => $date,
        ];
        $video_comment_store = $this->Video_model->video_comment_store($data);
        
        //PERHITUNGAN DAN INPUT NOTIFIKASI REPLY
        if ($getVideo[0]['user_id'] != null) {
            $cekNotif = $this->Video_model->video_notification_count($getVideo[0]['user_id'], $video_id, "reply")[0]['total'];
            if ($cekNotif == 0) {
                $dataNotif = [
                    'video_id' => $video_id,
                    'person_id' => $id_user_reply,
                    'from_person_id' => $id_user,
                    'video_notification_title' => "membalas komentar anda",
                    'video_notification_description' => $comment,
                    'video_notification_image' => $getVideo[0]['video_thumbnail'],
                    'video_notification_type' => 'reply',
                    'video_notification_date' => date('Y-m-d H:i:s'),
                    'comment_id' => $id_comment,
                    'comment_reply_id' => $video_comment_store,
                ];
                $this->Video_model->video_notification_store($dataNotif);
            }
        }
        
        $getComment = $this->Video_model->video_comment_reply($id_user, $video_id, null, $video_comment_store);
        // $video_comment_count = $this->Video_model->video_comment_count(null, $video_id);
        $video_comment_count = $this->Video_model->video_comment_count_level_one($video_id);
        $datasave = [
            'video_comment' => $video_comment_count,
        ];
        $video_update = $this->Video_model->video_update($video_id, $datasave);
        // print_r($video_update);
        // $data = $this->Video_model->get_data_user_by_person($id_user)[0];
        // $data['video_comment'] = $comment;
        // $data['video_id'] = $video_id;
        // $data['video_comment_status'] = 'reply';
        // $data['video_comment_reply_id'] = $id_comment;
        // $data['user_id_reply'] = $id_user_reply;
        $data = $getComment;
        $data[0]['convert_time'] = $this->convertTime($getComment[0]['video_comment_date_created']);
        if ($video_comment_store) {

            $this->response([
                'status' => true,
                'message' => 'OK',
                'data' => $data,
            ], REST_Controller::HTTP_OK);

        } else {

            $this->response([
                'status' => false,
                'message' => 'Gagal menambah komentar',
            ], REST_Controller::HTTP_OK);

        }

    }
    
    public function convertTime($datetime)
    {
        $time = strtotime($datetime);
        $diff = time() - $time;
        $diff /= 60;
        $var1 = floor($diff);
        $var = $var1 <= 1 ? 'menit' : 'menit';
        if ($diff >= 60) {
            $diff /= 60;
            $var1 = floor($diff);
            $var = $var1 <= 1 ? 'jam' : 'jam';
            if ($diff >= 24) {$diff /= 24;
                $var1 = floor($diff);
                $var = $var1 <= 1 ? 'hari' : 'hari';
                if ($diff >= 30.4375) {$diff /= 30.4375;
                    $var1 = floor($diff);
                    $var = $var1 <= 1 ? 'bulan' : 'bulan';
                    if ($diff >= 12) {$diff /= 12;
                        $var1 = floor($diff);
                        $var = $var1 <= 1 ? 'tahun' : 'tahun';}
                }
            }
        }
        return $var1 . ' ' . $var . ' yang lalu';
    }

}