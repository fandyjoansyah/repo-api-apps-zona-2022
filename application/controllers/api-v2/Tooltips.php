<?php

defined('BASEPATH') or exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Tooltips extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('All_model');
    }

    public function index_post()
    {
        $id_user = $this->input->post('id_user') != null ? $this->input->post('id_user') : null;
        $nama_tooltips = $this->input->post('nama_tooltips') != null ? $this->input->post('nama_tooltips') : null;

        if ($id_user == null) {
            $this->response([
                'status' => false,
                'message' => 'Data tidak valid',
            ], REST_Controller::HTTP_OK);
            exit();
        }
        $tooltips = $this->All_model->tooltips($id_user, $nama_tooltips);
        if ($tooltips) {

            $this->response([
                'status' => true,
                'message' => 'OK',
                'data' => $tooltips,
            ], REST_Controller::HTTP_OK);

        } else {

            $this->response([
                'status' => false,
                'message' => 'GAGAL',
            ], REST_Controller::HTTP_OK);

        }

    }

}