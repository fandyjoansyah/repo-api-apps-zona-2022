<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class Post_view extends REST_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('All_model');
        $this->load->library('user_agent');
    }


public function index_post(){
    $id_user = $this->input->post('id_user');
    $id_post = $this->input->post('id_post');
    // print_r("dadw");
    $useragent = $_SERVER['HTTP_USER_AGENT'];
    $arrAgent = [];
    preg_match('/Android (\d+)/i', $useragent, $arrAgent);
    $str_version = $arrAgent[0] ? $arrAgent[0] : 'Unknown OS';
    $ip = $this->input->ip_address();
    $date=date("Y-m-d");
    
    
    $cari_view=$this->All_model->get_view_post($id_user, $id_post, $date);
    // print_r($ip);
    if($cari_view){
        // cari ada 
        // respons
        $this->response([
            'status' => FALSE,
            'message' => 'Post FALSE'
        ], REST_Controller::HTTP_OK);
    }else{
        
        //ambil device name
        // if ($this->agent->is_browser()){
        //         $device = $this->agent->browser().' '.$this->agent->version();
        // }elseif ($this->agent->is_robot()){
        //         $device = $this->agent->robot();
        // }elseif ($this->agent->is_mobile()){
        //         $device = $this->agent->mobile();
        // }else{
        //         $device = 'Unidentified User Agent';
        // }
        $detect = new Mobile_Detect();

        if ($detect->isMobile() && !$detect->isTablet()) {
        $device = "Mobile";
        } else if (!$detect->isMobile() && $detect->isTablet()) {
            $device =  "Tablet";
        } else if ($detect->isiOS()) {
            $device =  "Ios";
        } else {
            $device =  "PC";
        }
        
        //cek ip di database
        $cekIp = $this->All_model->ip_address($ip, $date);
        if(count($cekIp) == 0){
            $result_curl = $this->curl_get('http://ip-api.com/json/'.$ip);
            $arrayIp = json_decode($result_curl, true);
            $dataIp = [
                'ip' => $ip,
                'country' => $arrayIp['country'],
                'address' => $arrayIp['region'] . ' ' . $arrayIp['regionName'] . ' ' . $arrayIp['city'] . ' ' . $arrayIp['zip'],
                'isp' => $arrayIp['isp'],
                'json' => $result_curl,
                'device' => $device,
                'date' => $date,
                ];
            $this->All_model->ip_address_store($dataIp);
        }
        
        // cari tidak ada
        // insert ke custom_post_view
        $data=[
            'id_user'=>$this->post('id_user'),
            'id_post'=>$this->post('id_post'),
            'tgl_view'=>$date,
            'device' => $device,
            'ip' => $ip,
            'os' => $str_version,
            'user_agent' => $useragent,
            ];  

        $insert_post_view=$this->All_model->insert_post_view($data);
        
        if($insert_post_view){
            // update custom_total_post_view
            
            $update_total_view=$this->All_model->update_post_total_view($id_post);

            if($update_total_view){
                
                $this->response([
                    'status' => TRUE,
                    'message' => 'Update OK',
                    'data'=>$update_total_view
                ], REST_Controller::HTTP_OK);

            }else{

                $this->response([
                    'status' => FALSE,
                    'message' => 'Update FALSE',
                    'data'=>$update_total_view
                ], REST_Controller::HTTP_OK);


            }

        }else{

            $this->response([
                'status' => FALSE,
                'message' => 'Insert FALSE'
            ], REST_Controller::HTTP_OK);

        }

    }




    
}


public function curl_get($url){
     $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
    $output = curl_exec($ch); 
    return $output;
}


    

}


?>