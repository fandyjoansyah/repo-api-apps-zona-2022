<?php



defined('BASEPATH') OR exit('No direct script access allowed');



// This can be removed if you use __autoload() in config.php OR use Modular Extensions

/** @noinspection PhpIncludeInspection */

//To Solve File REST_Controller not found

require APPPATH . 'libraries/REST_Controller.php';

require APPPATH . 'libraries/Format.php';





class Get_komentar_v2 extends REST_Controller{



    public function __construct(){

        parent::__construct();

        $this->load->model('All_model');

    }



public function index_post(){

    $id_post = $this->input->post('id_post');
    $id_user = $this->input->post('id_user');
   
    
    //query 1. dapatkan semua komentar dari id_post tertentu
    $komen=$this->All_model->get_komen($id_post);
    
    $subKomen;
    
  

    if($komen){
    //query 2.dapatkan semua jumlah like komentar dari per setiap id_komentar
    for($i = 0;$i<count($komen);$i++)
    {
        $responJumlahLikeDanLikePenggunaSekarangAdaAtauTidak = $this->All_model->getSemuaPenggunaYangLikeDanCekLikeKomentarPenggunaSekarangByIdKomentar($komen[$i]['id_komentar'],$id_user);
        $jumlahLike = count($responJumlahLikeDanLikePenggunaSekarangAdaAtauTidak['person_id']);
        $komen[$i]['jumlah_like'] = $jumlahLike;
        $komen[$i]['ada_like_current_user'] = $responJumlahLikeDanLikePenggunaSekarangAdaAtauTidak['ada_like'];
        
        //start dapatkan child komentar
        $subKomen = $this->All_model->getSubKomentarAndLike($komen[$i]['id_komentar']);
        $komen[$i]['sub_komentar'] = $subKomen;
        //end dapatkan child komentar
        
        
    }
    
    
    $komen = $this->formatWaktu($komen);
        $this->response([

            'status' => TRUE,

            'message' => 'Komen didapat',

            'item_terbaru' => $komen
            
            
        ], REST_Controller::HTTP_OK);

    }else{

        $this->response([

            'status' => FALSE,

            'message' => 'Komen didapat'
        ], REST_Controller::HTTP_OK);

    }

    

}



//start format waktu

function formatWaktu($data){
    for($i=0;$i<count($data);$i++)
    {
        $data[$i]['waktu_komentar'] = $this->olahTanggal($data[$i]['waktu_komentar']);
    }

    return $data;
}

function olahTanggal($tglArtikel)
{
    $tglSekarang = date('Y-m-d H:i:s');
    $tglSekarang = new DateTime($tglSekarang);
    $hasil_waktu = $tglSekarang->diff(new DateTime($tglArtikel));

    $hari = $hasil_waktu->d;
    $jam = $hasil_waktu->h;
    $menit = $hasil_waktu->i;
    $detik = $hasil_waktu->s;
    
    
    
    if($hari > 0 && $hari<= 14)
    {
        return $hari. ' hr';
    }
    else if($hari > 14)
    {
        return floor($hari/7). ' mg';
    }

    else if($jam > 0){
        return $jam.' jam';
    }
    else if($jam == 0){ //jika menit bukan nol
        if($menit != 0){
            return $menit.' menit';
        }
        else //jika menit = 0
        {
            return 'baru saja';
        }
    }

}




function waktuDefault($tglArtikel)
{
    
   // $tglArtikel =  date("d M Y | h:i", strtotime($tglArtikel));
    $tanggal = date("d",strtotime($tglArtikel));
    $bulan = $this->getBulanIndo(date("M",strtotime($tglArtikel)));
    $tahun = date("Y",strtotime($tglArtikel));
    $jam = date("H",strtotime($tglArtikel));
    $menit = date("i",strtotime($tglArtikel));
    
    return $tanggal." ".$bulan." ".$tahun." I ".$jam.":".$menit;
    
    
}

function getBulanIndo($bulan)
{
    switch($bulan)
    {
        case 'Jan' : 
        return 'Januari';
        case 'Feb' : 
        return 'Februari';
        case 'Mar' : 
        return 'Maret';
        case 'Apr' : 
        return 'April';
        case 'May' : 
        return 'Mei';
        case 'Jun' : 
        return 'Juni';
        case 'Jul' : 
        return 'Juli';
        case 'Aug' :
        return 'Agustus';
        case 'Sep' : 
        return 'September';
        case 'Oct' : 
        return 'Oktober';
        case 'Nov' : 
        return 'November';
        case 'Dec' : 
        return 'Desember';

    }
}



//start caller function
//rnd('2021-10-24 13:24:00');

//end caller function
//end format waktu

    



}





?>