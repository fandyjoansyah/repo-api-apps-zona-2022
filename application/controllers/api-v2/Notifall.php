<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class Notifall extends REST_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('All_model');
    }


    public function index_post(){

        $tittle=$this->post('tittle');
        $pesan=$this->post('pesan');
        $image=$this->post('image');
    
        $get_data=$this->All_model->notif_broadcase($tittle, $pesan, $image);
    
        if($get_data){
    
            $this->response([
                'status' => TRUE,
                'message' => 'Notif Sukses ',
                'Data '=>$get_data
            ], REST_Controller::HTTP_OK);
    
        }else{
            $this->response([
                'status' => FALSE,
                'message' => 'Notif Gagagl'
            ], REST_Controller::HTTP_OK);
        }
       }



    

}


?>