<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class Download_template extends REST_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('All_model');
        $this->load->library('user_agent');
    }


public function index_get(){
    $id_user = $this->input->get('id_user');
    $id_download = $this->input->get('id_download');
    
    $file = $this->All_model->get_file_by_id($id_download)[0];
    // print_r($this->curl($file['url_file']));
    $url = preg_replace('~[\r\n]+~', '', $file['url_file']);
    // $url = "http://superadmin.zonamahasiswa.id/File//2021-12-19_10_48_15.pdf";
    // print_r($url);
    // echo "<br>";
    // print_r($url2);
    // echo "<br>";
    if($url){
    // $explodeFile = explode('/', $url);
    // $fileName = $explodeFile[(count($explodeFile)-1)];
    
    // echo $fileName;
    //User Agent
    $useragent = $_SERVER['HTTP_USER_AGENT'];
    $arrAgent = [];
    preg_match('/Android (\d+)/i', $useragent, $arrAgent);
    // OS
    $str_version = $arrAgent[0] ? $arrAgent[0] : 'Unknown OS';
    // IP
    $ip = $this->input->ip_address();
    $date=date("Y-m-d H:i:s");
    
    //ambil device name
        if ($this->agent->is_browser()){
                $device = $this->agent->browser().' '.$this->agent->version();
        }elseif ($this->agent->is_robot()){
                $device = $this->agent->robot();
        }elseif ($this->agent->is_mobile()){
                $device = $this->agent->mobile();
        }else{
                $device = 'Unidentified User Agent';
        }
    
    $headers = get_headers($url, true);

        $par = parse_url($url);
        $hostname = $par['host'];
        $dir = FCPATH . "/temp/";
		$fname = basename($par['path']);
		if(file_exists($fname)){
			$fnameFix = rand()."-".$fname;
// 			$fnameFix = $fname;
		}else{
			$fnameFix = $fname;
		}
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Host: '.$hostname));
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.71 Safari/537.36");
		curl_setopt($ch, CURLOPT_TIMEOUT, 600);
		$rsc = curl_exec($ch);
		$cd = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$sz = curl_getinfo($ch, CURLINFO_SIZE_DOWNLOAD);
// 		print_r($cd);
		if($cd == 200){
// 			if($sz < 5242880){
		file_put_contents($dir.$fnameFix,$rsc);
				// logdl($tmp,$fnameFix);
		$ext = pathinfo($dlfile, PATHINFO_EXTENSION);
		header("content-disposition: attachment; filename=$fnameFix");
		header("content-type: application/octet-stream");
		$get = file_get_contents($dir.$fnameFix);
		print $get;
		
		$data = [
		    'id_user' => $id_user,
		    'id_download' => $id_download,
		    'device_download_log' => $device,
		    'ip_download_log' => $ip,
		    'os_download_log' => $str_version,
		    'user_agent_download_log' => $useragent,
		    'date_download_log' => $date,
		    ];
		$this->All_model->store_download_log($data);
				// print "<script>document.location = document.location;</script>";
// 			}else{
// 				print "max 5mb";
// 			}
		}else{
			print "error url";
		}
    }

}

}


?>