<?php



defined('BASEPATH') OR exit('No direct script access allowed');



// This can be removed if you use __autoload() in config.php OR use Modular Extensions

/** @noinspection PhpIncludeInspection */

//To Solve File REST_Controller not found

require APPPATH . 'libraries/REST_Controller.php';

require APPPATH . 'libraries/Format.php';





class Home_fix_dengan_waktu_rnd extends REST_Controller{



    public function __construct(){

        parent::__construct();

        $this->load->model('All_model');
        $this->load->library('user_agent');
        date_default_timezone_set('Asia/Jakarta');
    }









public function index_post(){

    $halamanaktif = $this->input->post('halamanaktif') < 1 ? 1 : $this->input->post('halamanaktif');
    $id_user = $this->input->post('id_user');
    $id_categori = "3142"; //id cerbung
    $halamanAktifHorizontal = 1;

    $date = date("Y-m-d");
    $date_time = date("Y-m-d H:i:s");
    $ip = $this->input->ip_address();
    
    //Cek Device
    $detect = new Mobile_Detect();

    if ($detect->isMobile() && !$detect->isTablet()) {
        $device = "Mobile";
    } else if (!$detect->isMobile() && $detect->isTablet()) {
        $device =  "Tablet";
    } else if ($detect->isiOS()) {
        $device =  "Ios";
    } else {
        $device =  "PC";
    }
    
    //cek ip di database
    $cekIp = $this->All_model->ip_address($ip, $date);
    if(count($cekIp) == 0){
        $result_curl = $this->curl_get('http://ip-api.com/json/'.$ip);
        $arrayIp = json_decode($result_curl, true);
        $dataIp = [
            'ip' => $ip,
            'country' => $arrayIp['country'],
            'address' => $arrayIp['region'] . ' ' . $arrayIp['regionName'] . ' ' . $arrayIp['city'] . ' ' . $arrayIp['zip'],
            'isp' => $arrayIp['isp'],
            'json' => $result_curl,
            'device' => $device,
            'date' => $date,
            ];
        $this->All_model->ip_address_store($dataIp);
    }

        $item_terbaru=$this->All_model->get_post_by_date_fix2_dengan_waktu_rnd($halamanaktif,$id_user);
        
    
        // cek apakah darai hasil ini ada yang di simpan ?

        if($item_terbaru){

        // barang ada
        
                //  $total_post_horizontal=$this->All_model->get_post_by_id_categori($id_categori);

                //  $horizontal=$this->All_model->get_post_by_id_categori_pagination($id_categori, $total_post_horizontal, $halamanAktifHorizontal);
           
           $horizontal = [];
           if($halamanaktif==1)
           {
           
                                    $horizontal=$this->All_model->get_post_by_id_categori_pagination_rnd($id_categori,$id_user);
           }  
           if($horizontal != [])
           {
                $horizontalPlusLike = $horizontal;
               //  $horizontalPlusLike = $this->All_model->getsavelike($horizontal, $id_user);  
                 
                 $horizontalPlusLike = $this->formatWaktu($horizontalPlusLike); 
           }
           else
           {
               $horizontalPlusLike = $horizontal;
           }
                

                $item_fix = $this->formatWaktu($item_terbaru);
                
             

               
                $this->response([

                    'status' => TRUE,

                    'message' => 'Home didapat',

                    'item_terbaru' => $item_fix, 
                    
                    'item_horizontal' => $horizontalPlusLike
                    
                   
                    

                ], REST_Controller::HTTP_OK);

            



        }else{



            // barang ada

            $this->response([

                'status' => FALSE,

                'message' => 'Home didapat'

            ], REST_Controller::HTTP_OK);



        }

}





//start format waktu

public function formatWaktu($data)
    {
        foreach ($data as $key => $value) {
            # code...
            $post_date = $value->post_date;
            if ($value->id_user_weslist != null) {
                $like = '1';
            } else {
                $like = '0';
            }
            $waktu = $this->olahTanggal($value->post_date);
            $data_baru[] = array('ID' => $value->ID, 'post_author' => $value->post_author, 'post_date' => $waktu,
                'post_title' => $value->post_title, 'guid' => $value->guid, 'post_content' => $value->post_content, 'meta_value' => $value->meta_value,
                'user_login' => $value->user_login, 'name' => $value->name, 'term_id' => $value->term_id, 'gambar_user_wp' => $value->gambar_user_wp, 'like' => $like);
        }

        // for ($i = 0; $i < count($data); $i++) {
        //     $data[$i]['post_date'] = $this->olahTanggal($data[$i]['post_date']);
        // }

        return $data_baru;
    }

function olahTanggal($tglArtikel)
{
$timePost = "2021-10-24 11:23:41";


$tglSekarang = date('Y-m-d H:i:s');
$tglSekarang = new DateTime($tglSekarang);
$hasil_waktu = $tglSekarang->diff(new DateTime($tglArtikel));
$jam = $hasil_waktu->h;
$menit = $hasil_waktu->i;
$detik = $hasil_waktu->s;


if($jam == 1){
    return $jam.' jam yang lalu';
}
else if($jam > 1){ //jika jam lebih dari 1
  return  $this->waktuDefault($tglArtikel);
}
else if($jam == 0){ //jika menit bukan nol
    if($menit != 0){
        return $menit.' menit yang lalu';
    }
    else //jika menit = 0
    {
        return "baru saja";
    }
}

}




function waktuDefault($tglArtikel)
{
    
   // $tglArtikel =  date("d M Y | h:i", strtotime($tglArtikel));
    $tanggal = date("d",strtotime($tglArtikel));
    $bulan = $this->getBulanIndo(date("M",strtotime($tglArtikel)));
    $tahun = date("Y",strtotime($tglArtikel));
    $jam = date("H",strtotime($tglArtikel));
    $menit = date("i",strtotime($tglArtikel));
    
    return $tanggal." ".$bulan." ".$tahun." I ".$jam.":".$menit;
    
    
}

function getBulanIndo($bulan)
{
    switch($bulan)
    {
        case 'Jan' : 
        return 'Januari';
        case 'Feb' : 
        return 'Februari';
        case 'Mar' : 
        return 'Maret';
        case 'Apr' : 
        return 'April';
        case 'May' : 
        return 'Mei';
        case 'Jun' : 
        return 'Juni';
        case 'Jul' : 
        return 'Juli';
        case 'Aug' :
        return 'Agustus';
        case 'Sep' : 
        return 'September';
        case 'Oct' : 
        return 'Oktober';
        case 'Nov' : 
        return 'November';
        case 'Dec' : 
        return 'Desember';

    }
}



//start caller function
//rnd('2021-10-24 13:24:00');

//end caller function
//end format waktu


public function curl_get($url){
     $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
    $output = curl_exec($ch); 
    return $output;
}

    



}





?>