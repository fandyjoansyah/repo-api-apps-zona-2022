<?php

defined('BASEPATH') or exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Tooltips_create extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('All_model');
    }

    public function index_post()
    {
        $id_user = $this->input->post('id_user') != null ? $this->input->post('id_user') : null;
        $nama_tooltips = $this->input->post('nama_tooltips') != null ? $this->input->post('nama_tooltips') : null;

        if ($id_user == null) {
            $this->response([
                'status' => false,
                'message' => 'Data tidak valid',
            ], REST_Controller::HTTP_OK);
            exit();
        }
        $getTooltips = $this->All_model->tooltips(null, $nama_tooltips);
        $cekTooltips = $this->All_model->tooltips($id_user, $nama_tooltips);
        $tooltips_insert = [
            'tooltips_id' => $getTooltips[0]['tooltips_id'],
            'user_id' => $id_user,
        ];
        $tooltips = true;
        if ($cekTooltips[0]['tooltips_click_id'] == null) {
            $tooltips = $this->All_model->tooltips_click_store($tooltips_insert);
        }
        if ($tooltips) {

            $this->response([
                'status' => true,
                'message' => 'OK',
                'data' => $tooltips_insert,
            ], REST_Controller::HTTP_OK);

        } else {

            $this->response([
                'status' => false,
                'message' => 'GAGAL',
            ], REST_Controller::HTTP_OK);

        }

    }

}