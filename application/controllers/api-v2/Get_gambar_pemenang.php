<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class Get_gambar_pemenang extends REST_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('All_model');
    }




public function index_post(){
    $pilihanMenang = $this->input->post('pilihan_menang');

    $urlGambarPemenang=$this->All_model->getGambarPemenang($pilihanMenang);

    if($urlGambarPemenang){

        $this->response([
            'status' => TRUE,
            'message' => 'Gambar Pemenang Ditemukan',
            'data' => $urlGambarPemenang
        ], REST_Controller::HTTP_OK);

    }else{

        $this->response([
            'status' => FALSE,
            'message' => 'Gambar Pemenang Tidak Ditemukan'
        ], REST_Controller::HTTP_OK);

    }
    

}


    

}


?>