<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class Rekomendasi extends REST_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('All_model');
    }




public function index_post(){
    $id_categori = $this->input->post('id_categori');
    $id_post = $this->input->post('id_post');
    $id_user = $this->input->post('id_user');

    $komen=$this->All_model->total_komentar($id_post);

    if($komen){
        $total_komentar=$komen;

    }else{
        $total_komentar="0";
    }


    $item_terbaru=$this->All_model->rekomendasi_post($id_categori, $id_post);

    if($item_terbaru){

        $item_fix=$this->All_model->getsavelike($item_terbaru, $id_user);

        if($item_fix){

            
            $this->response([

                'status' => TRUE,

                'message' => 'Home didapat',

                'item_terbaru' => $item_fix,
                'total_komentar'=>$total_komentar

            ], REST_Controller::HTTP_OK);

        }else{
            
            $this->response([

                'status' => TRUE,

                'message' => 'Home didapat',

                'item_terbaru' => $item_fix,
                'total_komentar'=>$total_komentar

            ], REST_Controller::HTTP_OK);
        }

    }else{

        $this->response([

            'status' => FALSE,

            'message' => 'REKOMENDASI FALSE'

        ], REST_Controller::HTTP_OK);
        
    }


    
}


    

}


?>