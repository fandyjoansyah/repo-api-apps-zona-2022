<?php
//FILE CREATOR : Fandy
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class Get_gambar_poling_trending extends REST_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('All_model');
    }




public function index_post(){

    $jumlahItem = $this->input->post('jumlahItem');
    $trending = $this->All_model->get_viewpager_gambar_poling_trending($jumlahItem);
    
    if($trending == null)
    {
        //jika trending = null, maka tampilkan gambar poling saja berjumlah 3 secara normal
        $trending = $this->All_model->get_viewpager_gambar_poling_normal($jumlahItem);
    }
    
        $this->response([
            'status' => TRUE,
            'message' => 'Polling Trending',
            'trending' => $trending
        ], REST_Controller::HTTP_OK);
}


    

}


?>