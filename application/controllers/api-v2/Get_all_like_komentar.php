<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class Get_all_like_komentar extends REST_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('All_model');
    }




public function index_post(){
    $person_id = $this->input->post('person_id');
    $id_komentar = $this->input->post('id_komentar');
    
    $semuaLikeDariKomentarTertentu=$this->All_model->getAllLikeKomentar($id_komentar,$person_id);

    if($semuaLikeDariKomentarTertentu){

        $this->response([
            'status' => TRUE,
            'message' => 'Save Like OK',
            'data' => $semuaLikeDariKomentarTertentu
        ], REST_Controller::HTTP_OK);

    }else{

        $this->response([
            'status' => FALSE,
            'message' => 'Save Like GGAL'
        ], REST_Controller::HTTP_OK);

    }
    

}


    

}


?>