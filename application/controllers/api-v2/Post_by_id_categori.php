<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class Post_by_id_categori extends REST_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('All_model');
    }




public function index_post(){
    $id_categori = $this->input->post('id_categori');
    
    $halamanaktif = $this->input->post('halamanaktif');
    $id_user = $this->input->post('id_user');

    if($halamanaktif<1){
        $halamanaktif=1;
    }

    $total_post=$this->All_model->get_post_by_id_categori($id_categori);



    if($total_post){

        $item_terbaru=$this->All_model->get_post_by_id_categori_pagination($id_categori, $total_post, $halamanaktif);

        if($item_terbaru){

            $item_fix=$this->All_model->getsavelike($item_terbaru, $id_user);

            if($item_fix){

                
                $this->response([

                    'status' => TRUE,

                    'message' => 'Home didapat',

                    'item_terbaru' => $item_fix

                ], REST_Controller::HTTP_OK);

            }else{
                            $this->response([

                'status' => FALSE,

                'message' => 'KAtegori False'

            ], REST_Controller::HTTP_OK);
            
            }

        }else{
            
            $this->response([

                'status' => FALSE,

                'message' => 'KAtegori False'

            ], REST_Controller::HTTP_OK);
            
            
        }
    }else{

        
            // barang ada

            $this->response([

                'status' => FALSE,

                'message' => 'KAtegori False'

            ], REST_Controller::HTTP_OK);

    }


    
}


    

}


?>