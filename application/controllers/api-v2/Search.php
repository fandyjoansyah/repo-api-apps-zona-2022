<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class Search extends REST_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('All_model');
    }




public function index_post(){
    $key_search = $this->input->post('key_search');

    $get_pos=$this->All_model->get_search($key_search);

    if($get_pos){

        // TAG OK
        $this->response([
            'status' => TRUE,
            'message' => 'POS OK',
            'data' => $get_pos
        ], REST_Controller::HTTP_OK);

    }else{

         // TAG FALSE
         $this->response([
            'status' => FALSE,
            'message' => 'POS FALSE',
            'data' => $get_pos
        ], REST_Controller::HTTP_OK);

    }
}


    

}


?>