<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class Get_notifikasi extends REST_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('All_model');
    }




public function index_get(){



    $get_notif=$this->All_model->get_notifikasi();

    
    if($get_notif){

        $this->response([
            'status' => TRUE,
            'message' => 'NOTIF OK',
            'data' => $get_notif
        ], REST_Controller::HTTP_OK);

    }else{

        $this->response([
            'status' => FALSE,
            'message' => 'NOTIF FALSE'
        ], REST_Controller::HTTP_OK);

    }
    

    

}


    

}


?>