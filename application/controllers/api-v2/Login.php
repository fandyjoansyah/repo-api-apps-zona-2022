<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class Login extends REST_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('All_model');
    }



    public function index_post(){

        $person_email = $this->input->post('person_email');
        $Token = $this->input->post('person_token');
        $personId = $this->input->post('person_id');

        $person_email_cek=$this->All_model->getUseremail($person_email);

        if($person_email_cek){

            // jika email sudah ada 
            // cek token email server dan token post 

            if($person_email_cek==$Token){
                // jika token sama 
                $get_data=$this->All_model->get_data_user($person_email);
                if($get_data){
                    // data ada 
                    $this->response([
                        'status' => TRUE,
                        'message' => 'Data Ada ',
                        'data'=>$get_data
                    ], REST_Controller::HTTP_OK);

                }else{
                    // data kosong
                    $this->response([
                        'status' => FALSE,
                        'message' => 'Data Kosong '
                    ], REST_Controller::HTTP_OK);
                }
            }else{
                // jika token tidak sama
                // update token
                $data=[
                    'person_token'=>$this->post('person_token')
                    ];   

                $update_token=$this->All_model->update_token($personId, $data);
                
                if($update_token){
                    
                    // ambil data user

                    $get_data=$this->All_model->get_data_user($person_email);
                    if($get_data){
                        // data ada 
                        $this->response([
                            'status' => TRUE,
                            'message' => 'Data Ada ',
                            'data'=>$get_data
                        ], REST_Controller::HTTP_OK);

                    }else{
                        // data kosong
                        $this->response([
                            'status' => FALSE,
                            'message' => 'Data Kosong '
                        ], REST_Controller::HTTP_OK);
                    }

                }else{

                    $this->response([
                        'status' => FALSE,
                        'message' => 'Data Kosong '
                    ], REST_Controller::HTTP_OK);
                }
            }

        }else{

            // jika email belum ada 
            // create user
            $data=[
                'id_user'=>"",
                'person_id'=>$this->post('person_id'),
                'person_name'=>$this->post('person_name'),
                'person_given_name'=>$this->post('person_given_name'),
                'person_family_name'=>$this->post('person_family_name'),
                'person_email'=>$this->post('person_email'),
                'person_photo'=>$this->post('person_photo'),
                'person_token'=>$this->post('person_token')  
            ];

            $create_user=$this->All_model->createUser($data);

            if($create_user){

                // sukses insert
                // ambil data user

                $get_data=$this->All_model->get_data_user($person_email);
                if($get_data){
                    // data ada 
                    $this->response([
                        'status' => TRUE,
                        'message' => 'Data Ada ',
                        'data'=>$get_data
                    ], REST_Controller::HTTP_OK);

                }else{
                    // data kosong
                    $this->response([
                        'status' => FALSE,
                        'message' => 'Data Kosong ',
                        'data'=>$get_data
                    ], REST_Controller::HTTP_OK);
                }

            }else{

                // gagal buat user
                $this->response([
                    'status' => FALSE,
                    'message' => 'Data Kosong '
                ], REST_Controller::HTTP_OK);
            }
        }
    }


    

}


?>