<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class Rekomendasi_dengan_waktu extends REST_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('All_model');
    }




public function index_post(){
    $id_categori = $this->input->post('id_categori');
    $id_post = $this->input->post('id_post');
    $id_user = $this->input->post('id_user');

    $komen=$this->All_model->total_komentar($id_post);

    if($komen){
        $total_komentar=$komen;

    }else{
        $total_komentar="0";
    }


    $item_terbaru=$this->All_model->rekomendasi_post($id_categori, $id_post);

    if($item_terbaru){

        $item_fix=$this->All_model->getsavelike($item_terbaru, $id_user);
        
        $item_fix=$this->formatWaktu($item_fix);

        if($item_fix){

            
            $this->response([

                'status' => TRUE,

                'message' => 'Home didapat',

                'item_terbaru' => $item_fix,
                'total_komentar'=>$total_komentar

            ], REST_Controller::HTTP_OK);

        }else{
            
            $this->response([

                'status' => TRUE,

                'message' => 'Home didapat',

                'item_terbaru' => $item_fix,
                'total_komentar'=>$total_komentar

            ], REST_Controller::HTTP_OK);
        }

    }else{

        $this->response([

            'status' => FALSE,

            'message' => 'REKOMENDASI FALSE'

        ], REST_Controller::HTTP_OK);
        
    }


    
}

//start format waktu

function formatWaktu($data){
    for($i=0;$i<count($data);$i++)
    {
        $data[$i]['post_date'] = $this->olahTanggal($data[$i]['post_date']);
    }

    return $data;
}

function olahTanggal($tglArtikel)
{
$timePost = "2021-10-24 11:23:41";


$tglSekarang = date('Y-m-d H:i:s');
$tglSekarang = new DateTime($tglSekarang);
$hasil_waktu = $tglSekarang->diff(new DateTime($tglArtikel));
$jam = $hasil_waktu->h;
$menit = $hasil_waktu->i;
$detik = $hasil_waktu->s;


if($jam == 1){
    return $jam.' jam yang lalu';
}
else if($jam > 1){ //jika jam lebih dari 1
  return  $this->waktuDefault($tglArtikel);
}
else if($jam == 0){ //jika menit bukan nol
    if($menit != 0){
        return $menit.' menit yang lalu';
    }
    else //jika menit = 0
    {
        return "baru saja";
    }
}

}




function waktuDefault($tglArtikel)
{
    
   // $tglArtikel =  date("d M Y | h:i", strtotime($tglArtikel));
    $tanggal = date("d",strtotime($tglArtikel));
    $bulan = $this->getBulanIndo(date("M",strtotime($tglArtikel)));
    $tahun = date("Y",strtotime($tglArtikel));
    $jam = date("H",strtotime($tglArtikel));
    $menit = date("i",strtotime($tglArtikel));
    
    return $tanggal." ".$bulan." ".$tahun." I ".$jam.":".$menit;
    
    
}

function getBulanIndo($bulan)
{
    switch($bulan)
    {
        case 'Jan' : 
        return 'Januari';
        case 'Feb' : 
        return 'Februari';
        case 'Mar' : 
        return 'Maret';
        case 'Apr' : 
        return 'April';
        case 'May' : 
        return 'Mei';
        case 'Jun' : 
        return 'Juni';
        case 'Jul' : 
        return 'Juli';
        case 'Aug' :
        return 'Agustus';
        case 'Sep' : 
        return 'September';
        case 'Oct' : 
        return 'Oktober';
        case 'Nov' : 
        return 'November';
        case 'Dec' : 
        return 'Desember';

    }
}



//start caller function
//rnd('2021-10-24 13:24:00');

//end caller function
//end format waktu
    

}


?>