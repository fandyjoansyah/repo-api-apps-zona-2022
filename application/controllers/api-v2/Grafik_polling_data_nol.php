<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class Grafik_polling_data_nol extends CI_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('All_model');
    }


    public function index()
    {
        $param = $this->input->get('get_pengguna');
        echo $param;
       //$this->load->view('grafik');
    }
    

    
    
    public function show($id_poling){
     $get_detail_poling=$this->All_model->get_detail_poling($id_poling);
     $arrColor = ['#a273d9','#9c450e','#14034a','#a24fa4','#bbc929','#49725a','#7b44fc','#d2a67d','#1c0ef9','#3627b6','#be77a0','#e917ca','#736a10','#af13f5','#055863','#a5cc31','#e8a32b','#a043da','#8e50f2','#71d966'];         
     $this->load->view('grafik_semua_nol',['data' => $get_detail_poling, 'color' => $arrColor]);
    }
    

}


?>