<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class Get_status_notif_update extends REST_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('All_model');
    }




public function index_get(){
    $statUpdate = $this->All_model->getStatusNotifUpdate();

    if($statUpdate){

        $this->response([
            'status_popup_update' => $statUpdate
        ], REST_Controller::HTTP_OK);

    }else{

        $this->response([
             'status_popup_update' => 0
        ], REST_Controller::HTTP_OK);

    }
    

}


    

}


?>