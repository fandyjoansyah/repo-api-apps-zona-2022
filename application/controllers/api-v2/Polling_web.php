<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class Polling_web extends REST_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('All_model');
    }
    
public function index_get(){


    $get_poling=$this->All_model->get_polling_for_web();
    
    if($get_poling){
        for($i = 0; $i < count($get_poling); $i++){
            $get_poling[$i]['polling'] = $this->All_model->get_polling_data($get_poling[$i]['id_poling']);
        }
        // TAG OK
        $this->response([
            'status' => TRUE,
            'message' => 'POS OK',
            'data' => $get_poling
        ], REST_Controller::HTTP_OK);

    }else{

         // TAG FALSE
         $this->response([
            'status' => FALSE,
            'message' => 'POS FALSE',
            'data' => ''
        ], REST_Controller::HTTP_OK);

    }
}

// public function index_get(){
//       $this->response([
//             'status' => TRUE,
//             'message' => 'pol web'
//         ], REST_Controller::HTTP_OK);
// }


    

}


?>