<?php



defined('BASEPATH') OR exit('No direct script access allowed');



// This can be removed if you use __autoload() in config.php OR use Modular Extensions

/** @noinspection PhpIncludeInspection */

//To Solve File REST_Controller not found

require APPPATH . 'libraries/REST_Controller.php';

require APPPATH . 'libraries/Format.php';





class Delete_sub_komentar extends REST_Controller{



    public function __construct(){

        parent::__construct();

        $this->load->model('All_model');

    }



public function index_post(){

    $data=[
        'id_sub_komentar'=>$this->post('id_sub_komentar'),
        'id_user'=>$this->post('id_user')
    ];

    

    $komen=$this->All_model->delete_sub_komentar($data);


    if($komen > 0){

        $this->response([

            'status' => TRUE,

            'message' => 'Sub Komen Dihapus',

            'item_terbaru' => $komen

        ], REST_Controller::HTTP_OK);

    }else{

        $this->response([

            'status' => FALSE,
            'message' => 'sub komen gagal dihapus'
        ], REST_Controller::HTTP_OK);

    }

    

}





    



}





?>