<?php



defined('BASEPATH') OR exit('No direct script access allowed');



// This can be removed if you use __autoload() in config.php OR use Modular Extensions

/** @noinspection PhpIncludeInspection */

//To Solve File REST_Controller not found

require APPPATH . 'libraries/REST_Controller.php';

require APPPATH . 'libraries/Format.php';





class User_online extends REST_Controller{



    public function __construct(){

        parent::__construct();

        $this->load->model('All_model');
        $this->load->library('user_agent');
    }



public function index_post(){

    $id_user = $this->input->post('id_user') != null ? $this->input->post('id_user') : null;
    $minute = $this->input->post('minute') != null ? $this->input->post('minute') : null;
    $date = date("Y-m-d");
    $date_time = date("Y-m-d H:i:s");
    $ip = $this->input->ip_address();
    
    //Cek Device
    $detect = new Mobile_Detect();

    if ($detect->isMobile() && !$detect->isTablet()) {
        $device = "Mobile";
    } else if (!$detect->isMobile() && $detect->isTablet()) {
        $device =  "Tablet";
    } else if ($detect->isiOS()) {
        $device =  "Ios";
    } else {
        $device =  "PC";
    }
    
    //cek ip di database
    $cekIp = $this->All_model->ip_address($ip, $date);
    if(count($cekIp) == 0){
        $result_curl = $this->curl_get('http://ip-api.com/json/'.$ip);
        $arrayIp = json_decode($result_curl, true);
        $dataIp = [
            'ip' => $ip,
            'country' => $arrayIp['country'],
            'address' => $arrayIp['region'] . ' ' . $arrayIp['regionName'] . ' ' . $arrayIp['city'] . ' ' . $arrayIp['zip'],
            'isp' => $arrayIp['isp'],
            'json' => $result_curl,
            'device' => $device,
            'date' => $date,
            ];
        $this->All_model->ip_address_store($dataIp);
    }
        
    //cek ke database apakah ada atau tidak
    $cekUserOnline = $this->All_model->last_user_online($id_user, $date);
    $res = null;
    if(count($cekUserOnline) == 0){
        // jika tidak ada create baru
        $data = [
            'person_id' => $id_user,
            'minute_online' => 0,
            'date_create' => $date_time,
            'date_update' => $date_time
            ];
        $res = $this->All_model->store_user_online($data);
    }else{
        $userOnline = $cekUserOnline[0];
        $dateCek = strtotime('-3 minutes', strtotime($date_time));
        $dateUpdate = strtotime($userOnline['date_update']);
        if(floor($dateUpdate) < floor($dateCek)){
            // jika masih kurang 3 menit maka update ke database
            $hitungMenit = $userOnline['minute_online'] + $minute;
            $data = [
                'minute_online' => $hitungMenit,
                'date_update' => $date_time
                ];
            $res = $this->All_model->update_user_online($userOnline['id'], $data);
        }else{
            // jika lebih 3 menit maka bikin create baru
            $data = [
                'person_id' => $id_user,
                'minute_online' => 0,
                'date_create' => $date_time,
                'date_update' => $date_time
                ];
            $res = $this->All_model->store_user_online($data);
        }
    }
  

    if($res){
        
        $this->response([

            'status' => TRUE,

            'message' => 'Berhasil menyimpan waktu',

        ], REST_Controller::HTTP_OK);

    }else{

        $this->response([

            'status' => FALSE,

            'message' => 'Gagal menyimpan waktu'
        ], REST_Controller::HTTP_OK);

    }

    

}

public function curl_get($url){
     $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
    $output = curl_exec($ch); 
    return $output;
}



    



}





?>