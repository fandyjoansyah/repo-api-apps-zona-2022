<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class Upload extends REST_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('All_model');
    }



	public function notifikasi_email($user_id){
	    $this->load->library('email');

        //start query model dapatkan data upload karya
        $hasil = $this->All_model->get_data_notifikasi_upload_karya($user_id);
        //end query model dapatkan data upload karya

        //start initiating email
        $this->email->set_newline("\r\n");
        $this->email->from('rubriksobatzona@zonamahasiswa.id','Rubrik Sobat Zona');
        $this->email->to('redaksi@zonamahasiswa.id');
         $this->email->set_mailtype("html");
        $this->email->bcc('fandyjoansyah@gmail.com','samfandxev@gmail.com');
        $this->email->subject('Kiriman Karya Rubrik Sobat Zona');
        $this->email->message("Hai! ada karya terbaru yang baru saja diupload oleh pengguna dari apps zona mahasiswa <br><br> 
         Nama Pengirim: <b>".$hasil[0]['person_name']."</b><br> 
          Email Pengirim: <b>".$hasil[0]['person_email']."</b>");
       $urlFile = $hasil[0]['url_file'];
       //mendapatkan semua text (nama file) setelah karakter "uploadedFiles/"
       $namaFile = substr($urlFile, strpos($urlFile, "uploadedFiles/") + 14);
	   $fileForEmail = $_SERVER["DOCUMENT_ROOT"]."/uploadedFiles/".$namaFile;
        $this->email->attach($fileForEmail); 
        //end initiating email
        

        //start kirim email
          if ($this->email->send()) {
           //berhasil upload
        } else {
            show_error($this->email->print_debugger());
        }
        //end kirim email
    
	}



    public function index_post(){


        $id_user=$_POST['id_user'];

        $originalImgNamex= $_FILES['filename']['name'];
        $originalImgName=date("Y-m-d-H:i:s").".doc";
        $tempName= $_FILES['filename']['tmp_name'];
        $folder="uploadedFiles/";
        //$url = "http://webtes.sahabat.or.id/".$originalImgName; //update path as per your directory structure 
        $folder="uploadedFiles/";
        $url = "http://api.zonamahasiswa.id/".$originalImgName;

        $urlfix="http://api.zonamahasiswa.id/uploadedFiles/".$originalImgName;

        if(move_uploaded_file($tempName,$folder.$originalImgName)){


            $data=[
                'id_file'=>"",
                'id_user'=>$this->post('id_user'),
                'url_file'=>$urlfix
            ];

            $create_file=$this->All_model->createFile($data);

            if($create_file){
                //notifikasi email ada yang upload
                $this->notifikasi_email($this->post('id_user'));
                
                $this->response([
                    'status' => TRUE,
                    'message' => 'Data FILE OK '.$this->post('id_user'),
                    'url'=>$urlfix,
                    'name_file'=>$originalImgName
                ], REST_Controller::HTTP_OK);

            }else{
                
                $this->response([
                    'status' => FALSE,
                    'message' => 'Data FILE FALSE ',
                    'url'=>$urlfix,
                    'name_file'=>$originalImgName
                ], REST_Controller::HTTP_OK);
            }
                

        	//echo "moved to ".$url;
        }else{
            $this->response([
                'status' => FALSE,
                'message' => 'Data Soal Ada '
            ], REST_Controller::HTTP_OK);
            
        }


        
    }


    

}


?>