<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class Get_detail_poling extends REST_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('All_model');
    }




public function index_post(){

    $id_poling = $this->input->post('id_poling');
    $id_user = $this->input->post('id_user');
    
    
    //start code get label untuk textview
    $label = $this->All_model->get_label_detail_poling($id_poling);
    //end code get label untuk textview
    
    

    $cek_poling=$this->All_model->cek_poling_fix($id_user, $id_poling);    

    if($cek_poling){
        // user sudah memilih
        $status_user="1";

    }else{
        $status_user="0";
    }

    $get_detail_poling=$this->All_model->get_detail_poling($id_poling);
    
    $selected_detail_polling = false;
    //start cek di dataset get_detail_polling, apakah ada seleksi user atau tidak.
    for($i=0;$i<count($get_detail_poling);$i++)
    {
        $alasan = array();
        if($get_detail_poling[$i]['id_detail_poling'] == $cek_poling[0]['id_detail_poling'])
        {
            $alasan = $this->All_model->get_alasan_poling($id_poling,$id_user);
            $selected_detail_polling = true;
        }
        else{
            $alasan[0]['alasan'] = "";
            $selected_detail_polling = false;
        }
        $get_detail_poling[$i]['selected'] = $selected_detail_polling;
        
        //start ambil alasan polling sebelumnya jika ada
            
            $get_detail_poling[$i]['alasan'] = $alasan[0]['alasan'];
        //end ambil alasan polling sebelumnya jika ada
    }
    //end cek di dataset get_detail_polling, apakah ada seleksi user atau tidak.
    
    
    

    
    if($get_detail_poling){
        
        $this->response([
            'status' => TRUE,
            'message' => 'Detail Poling OK',
            'data' => $get_detail_poling,
            'status_user'=>$status_user,
            'label' => $label['keterangan_untuk_textview'],
            'id_detail_poling_pilihan_user' => $cek_poling[0]['id_detail_poling']
        ], REST_Controller::HTTP_OK);

    }else{

        $this->response([
            'status' => FALSE,
            'message' => 'Total Poling FALSE'
        ], REST_Controller::HTTP_OK);

    }
    

    

}


    

}


?>