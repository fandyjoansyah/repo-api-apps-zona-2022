<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class Menyukai_artikel extends REST_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('All_model');
    }




public function index_post(){
    $id_user = $this->input->post('id_user');
    $id_post = $this->input->post('id_post');


    $select_save=$this->All_model->setMenyukaiOrDeleteMenyukaiIfExist($id_user, $id_post);

     if($select_save == 1){
        $this->response([
            'status' => TRUE,
            'message' => 'Berhasil Menambahkan Like'
        ], REST_Controller::HTTP_OK);
    }
    else if($select_save == 2){
           $this->response([
            'status' => TRUE,
            'message' => 'Berhasil Menghapus Like'
        ], REST_Controller::HTTP_OK);
        
    }
    else{
            $this->response([
            'status' => FALSE,
            'message' => 'Gagal'
        ], REST_Controller::HTTP_OK);
    }

    

}


    

}


?>