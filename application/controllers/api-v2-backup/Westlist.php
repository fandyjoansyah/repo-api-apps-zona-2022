<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class Westlist extends REST_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('All_model');
    }




public function index_post(){
    $id_user = $this->input->post('id_user');

    $get_pos=$this->All_model->westlist_user($id_user);

    if($get_pos){

        // TAG OK
        $this->response([
            'status' => TRUE,
            'message' => 'POS OK',
            'data' => $get_pos
        ], REST_Controller::HTTP_OK);

    }else{

         // TAG FALSE
         $this->response([
            'status' => FALSE,
            'message' => 'POS FALSE',
            'data' => $get_pos
        ], REST_Controller::HTTP_OK);

    }
}


    

}


?>