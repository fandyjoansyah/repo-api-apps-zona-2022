<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class Tag_by_id_post extends REST_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('All_model');
    }




public function index_post(){
    $id_post = $this->input->post('id_post');

    $get_tag=$this->All_model->get_tag_by_id_post($id_post);

    if($get_tag){

        // TAG OK
        $this->response([
            'status' => TRUE,
            'message' => 'TAG OK',
            'data' => $get_tag
        ], REST_Controller::HTTP_OK);

    }else{

         // TAG FALSE
         $this->response([
            'status' => FALSE,
            'message' => 'TAG FALSE',
            'data' => $get_tag
        ], REST_Controller::HTTP_OK);

    }
}


    

}


?>