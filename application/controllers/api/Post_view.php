<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class Post_view extends REST_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('All_model');
    }




public function index_post(){

    $id_user = $this->input->post('id_user');
    $id_post = $this->input->post('id_post');

    $date=date("Y-m-d");

    

    $cari_view=$this->All_model->get_view_post($id_user, $id_post, $date);

   
    if($cari_view){
        // cari ada 
        // respons
        $this->response([
            'status' => FALSE,
            'message' => 'Post FALSE'
        ], REST_Controller::HTTP_OK);
    }else{
        // cari tidak ada
        // insert ke custom_post_view
        $data=[
            'id_user'=>$this->post('id_user'),
            'id_post'=>$this->post('id_post'),
            'tgl_view'=>$date

            ];  

        $insert_post_view=$this->All_model->insert_post_view($data);
        if($insert_post_view){
            // update custom_total_post_view
            
            $update_total_view=$this->All_model->update_post_total_view($id_post);

            if($update_total_view){
                
                $this->response([
                    'status' => TRUE,
                    'message' => 'Update OK',
                    'data'=>$update_total_view
                ], REST_Controller::HTTP_OK);

            }else{

                $this->response([
                    'status' => FALSE,
                    'message' => 'Update FALSE',
                    'data'=>$update_total_view
                ], REST_Controller::HTTP_OK);


            }

        }else{

            $this->response([
                'status' => FALSE,
                'message' => 'Insert FALSE'
            ], REST_Controller::HTTP_OK);

        }

    }




    
}


    

}


?>