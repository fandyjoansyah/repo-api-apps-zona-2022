<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class My_tester extends CI_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('All_model');
    }


    public function index()
    {
        echo json_encode($this->All_model->get_detail_poling(16));
    }
    

}


?>