<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class Get_poling extends REST_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('All_model');
    }




public function index_get(){
    
    $jumlahData = $this->input->get('jumlahData');
    if($jumlahData == null){ //aksi jika get jumlahData = 0. (biasanya aplikasi versi lama dari java foundation)
        $jumlahData = 3;
    }
    

    $date=date("Y-m-d", strtotime(' +1 day'));

    $get_poling=$this->All_model->get_poling($date);

    $data_viewpager = $this->All_model->get_gambar_viewpager($jumlahData);
    
    if($get_poling){

        $this->response([
            'status' => TRUE,
            'message' => 'Poling OK',
            'data' => $get_poling,
            'data_viewpager' => $data_viewpager
        ], REST_Controller::HTTP_OK);

    }else{

        $this->response([
            'status' => FALSE,
            'message' => 'Poling FALSE',
            'Dat' => $date,
            'get_polling' => $get_poling
        ], REST_Controller::HTTP_OK);

    }
    

    

}


    

}


?>