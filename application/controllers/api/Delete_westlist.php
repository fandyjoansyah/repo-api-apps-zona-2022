<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class Delete_westlist extends REST_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('All_model');
    }




public function index_post(){
 //   $id_user = $this->input->post('id_user');
    $data=[
    
        'id_user'=>$this->post('id_user'),
        'id_post'=>$this->post('id_post')
    ];


    
    $create_westlist=$this->All_model->deletewestlist_ok($data);

       if($create_westlist){

        $this->response([
            'status' => TRUE,
            'message' => 'weslist OK',
            'data' => $create_westlist
        ], REST_Controller::HTTP_OK);

       }else{
            $this->response([
            'status' => FALSE,
            'message' => 'weslist GAGAL'
        ], REST_Controller::HTTP_OK);
       }
}


    

}


?>