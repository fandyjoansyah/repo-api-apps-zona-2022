<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class Menyukai_sub_komentar extends REST_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('All_model');
    }




public function index_post(){
    $id_sub_komentar = $this->input->post('id_sub_komentar');
    $person_id = $this->input->post('person_id');

    
    $statusLike=$this->All_model->setLikeSubKomentarOrDeleteLikeSubKomentarIfExist($id_sub_komentar, $person_id);

     if($statusLike == 1){
        $this->response([
            'status' => TRUE,
            'message' => 'Berhasil Menambahkan Like sub Komentar'
        ], REST_Controller::HTTP_OK);
    }
    else if($statusLike == 2){
           $this->response([
            'status' => TRUE,
            'message' => 'Berhasil Menghapus Like sub Komentar'
        ], REST_Controller::HTTP_OK);
        
    }
    else{
            $this->response([
            'status' => FALSE,
            'message' => 'Gagal'
        ], REST_Controller::HTTP_OK);
    }

}


    

}


?>