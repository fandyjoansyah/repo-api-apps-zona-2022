<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class Wp_notif extends REST_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('All_model');
    }




public function index_post(){

    $id_post=$this->input->post('id_post');

    $cek_custom_notif=$this->All_model->cek_custom_notif($id_post);

    if($cek_custom_notif){
        // ada
        $this->response([
            'status' => TRUE,
            'message' => 'Data Ada',
            'Data '=>$cek_custom_notif
        ], REST_Controller::HTTP_OK);
    }else{

        // Tidak ada
        $item_atas=$this->All_model->get_post_by_date_fix_limit_1($id_post);

        if($item_atas){

            foreach ($item_atas as $row)
            {
                    $tittle= $row['post_title'];
                    $pesan= "New Post | ".$row['post_title'];
                    $image= "https://zonamahasiswa.id/wp-content/uploads/".$row['meta_value'];
                    $namaKategori = $row['name'];
            }
       
            $data=[
                'id_notifikasi'=>"",
                'title_notifikasi'=>$tittle,
                'img_notifikasi'=>$image,
                'jenis_notifikasi'=>$namaKategori,
                'id_reverensi'=>$id_post
                
            ];
            $save_notifikasi=$this->All_model->save_notifikasi($data);


        
            $get_data=$this->All_model->notif_broadcase($tittle, $pesan, $image);

        }else{
            $get_data=false;
        }

    
        if($get_data){
    
            $this->response([
                'status' => TRUE,
                'message' => 'Notif Sukses ',
                'Data '=>$get_data
            ], REST_Controller::HTTP_OK);
    
        }else{
            $this->response([
                'status' => FALSE,
                'message' => 'Notif Gagagl'
            ], REST_Controller::HTTP_OK);
        }

    }

        

}


    

}


?>