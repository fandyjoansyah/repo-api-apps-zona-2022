<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class Search_fix extends REST_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('All_model');
    }




public function index_post(){

    $key = $this->input->post('key');
    
    $halamanaktif = $this->input->post('halamanaktif');
    $id_user = $this->input->post('id_user');

    if($halamanaktif<1){
        $halamanaktif=1;
    }

    $total_post=$this->All_model->search_fix($key);
   
    if($total_post){

        $item_terbaru=$this->All_model->search_fix_panigation($key, $total_post, $halamanaktif);

        if($item_terbaru){

            $item_fix=$this->All_model->getsavelike($item_terbaru, $id_user);

            if($item_fix){

                
                $this->response([

                    'status' => TRUE,

                    'message' => 'Search OK',

                    'item_terbaru' => $item_fix

                ], REST_Controller::HTTP_OK);

            }

        }else{
            
        }
    }else{

        
            // barang ada

            $this->response([

                'status' => FALSE,

                'message' => 'Search False'

            ], REST_Controller::HTTP_OK);

    }


    
}


    

}


?>