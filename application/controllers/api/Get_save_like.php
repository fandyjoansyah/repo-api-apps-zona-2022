<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class Get_save_like extends REST_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('All_model');
    }




public function index_post(){
    $id_user = $this->input->post('id_user');
    
    $item_terbaru=$this->All_model->get_post_save($id_user);

    if($item_terbaru){

        $this->response([
            'status' => TRUE,
            'message' => 'Save Like OK',
            'data' => $item_terbaru
        ], REST_Controller::HTTP_OK);

    }else{

        $this->response([
            'status' => FALSE,
            'message' => 'Save Like GGAL'
        ], REST_Controller::HTTP_OK);

    }
    

}


    

}


?>